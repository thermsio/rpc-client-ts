## [3.3.9](https://bitbucket.org/thermsio/rpc-client-ts/compare/v3.3.8...v3.3.9) (2025-03-07)


### Bug Fixes

* fix require remove ([139a1d3](https://bitbucket.org/thermsio/rpc-client-ts/commits/139a1d38fdcc4b05809a4f5089dcb23b3a89a394))

## [3.3.8](https://bitbucket.org/thermsio/rpc-client-ts/compare/v3.3.7...v3.3.8) (2025-03-07)


### Bug Fixes

* **WebSocketTransport:** only send server msg when connected via readyState check ([faf7ed5](https://bitbucket.org/thermsio/rpc-client-ts/commits/faf7ed5b6e7a9cd0dfc2ec2c2b4e6b94ff4ee9bc))

## [3.3.7](https://bitbucket.org/thermsio/rpc-client-ts/compare/v3.3.6...v3.3.7) (2025-03-07)


### Bug Fixes

* RPCClient debug msg for ws send srvr msg ([aa8dcc7](https://bitbucket.org/thermsio/rpc-client-ts/commits/aa8dcc7010ed7bdb553e22b1306d116781f5b8fd))

## [3.3.6](https://bitbucket.org/thermsio/rpc-client-ts/compare/v3.3.5...v3.3.6) (2025-03-07)


### Bug Fixes

* **WebSocketTransport:** only send server msg when connected ([84c7f7c](https://bitbucket.org/thermsio/rpc-client-ts/commits/84c7f7ca9d60ac0627e39eeb69a2c8a461bf117e))

## [3.3.5](https://bitbucket.org/thermsio/rpc-client-ts/compare/v3.3.4...v3.3.5) (2025-03-06)


### Bug Fixes

* **WebSocketTransport:** send client msg to server parsing ([06d0624](https://bitbucket.org/thermsio/rpc-client-ts/commits/06d0624aa20cd9189917fc1dba13ff6b3531bcfe))

## [3.3.4](https://bitbucket.org/thermsio/rpc-client-ts/compare/v3.3.3...v3.3.4) (2025-03-06)


### Bug Fixes

* **WebSocketTransport:** send client msg to server parsing ([d9727cd](https://bitbucket.org/thermsio/rpc-client-ts/commits/d9727cd282ae5120b06573e6e2779833ee73c4de))

## [3.3.3](https://bitbucket.org/thermsio/rpc-client-ts/compare/v3.3.2...v3.3.3) (2025-03-06)


### Bug Fixes

* **CallManager:** transports debug ([68abf96](https://bitbucket.org/thermsio/rpc-client-ts/commits/68abf961fc9d9c022a4e80b3b2263bd5e65c5b79))

## [3.3.2](https://bitbucket.org/thermsio/rpc-client-ts/compare/v3.3.1...v3.3.2) (2025-03-06)


### Bug Fixes

* **WebsocketTransport:** type and other polishing ([4815907](https://bitbucket.org/thermsio/rpc-client-ts/commits/481590769f782b3fd61f1fa493040751994c1fce))

## [3.3.1](https://bitbucket.org/thermsio/rpc-client-ts/compare/v3.3.0...v3.3.1) (2025-02-25)


### Bug Fixes

* add TransportType export ([2fdaca6](https://bitbucket.org/thermsio/rpc-client-ts/commits/2fdaca638b30a6b7ce0dc9b1de0d8bfd448b3c93))

# [3.3.0](https://bitbucket.org/thermsio/rpc-client-ts/compare/v3.2.2...v3.3.0) (2025-02-25)


### Features

* RPCClient transportOptions.prefferedTransport ([6b6b415](https://bitbucket.org/thermsio/rpc-client-ts/commits/6b6b415a3f61a1335e61e68bf55b001a050b2376))

## [3.2.2](http://bitbucket.org/thermsio/rpc-client-ts/compare/v3.2.1...v3.2.2) (2024-12-11)


### Bug Fixes

* udpate deps ([4e5409a](http://bitbucket.org/thermsio/rpc-client-ts/commits/4e5409aa55ed04017c1134dc71dab78802641c7a))

## [3.2.1](http://bitbucket.org/thermsio/rpc-client-ts/compare/v3.2.0...v3.2.1) (2024-09-03)


### Bug Fixes

* package.json main entry to cjs ([95ef3f7](http://bitbucket.org/thermsio/rpc-client-ts/commits/95ef3f7846f08b445657a2f26d704c8b510744e2))

# [3.2.0](http://bitbucket.org/thermsio/rpc-client-ts/compare/v3.1.0...v3.2.0) (2024-07-23)


### Features

* **RPCClientOptions:** allow ws transport autoConnect bool, resetConnection logic ([47446c7](http://bitbucket.org/thermsio/rpc-client-ts/commits/47446c70e4418ed0d91602acc229b43ca4653747))

# [3.1.0](http://bitbucket.org/thermsio/rpc-client-ts/compare/v3.0.5...v3.1.0) (2024-07-23)


### Features

* **RPCClientOptions:** allow ws transport autoConnect bool ([d375091](http://bitbucket.org/thermsio/rpc-client-ts/commits/d3750911b52e01f915282cfc5ab0951b79e67439))

## [3.0.5](http://bitbucket.org/thermsio/rpc-client-ts/compare/v3.0.4...v3.0.5) (2024-07-23)


### Bug Fixes

* update deps ([88b2224](http://bitbucket.org/thermsio/rpc-client-ts/commits/88b22243acbfec225bed769ce2fd6af969a35a04))

## [3.0.4](http://bitbucket.org/thermsio/rpc-client-ts/compare/v3.0.3...v3.0.4) (2024-07-15)


### Bug Fixes

* update deps ([70b845f](http://bitbucket.org/thermsio/rpc-client-ts/commits/70b845f27d9ef01fd08e92591d211cdd5264c40a))

## [3.0.3](https://bitbucket.org/thermsio/rpc-client-ts/compare/v3.0.2...v3.0.3) (2024-06-20)


### Bug Fixes

* build dist ([6186fff](https://bitbucket.org/thermsio/rpc-client-ts/commits/6186fff645d4b6c7067fae0b96dd28643fa861bf))

## [3.0.2](https://bitbucket.org/thermsio/rpc-client-ts/compare/v3.0.1...v3.0.2) (2024-06-20)


### Bug Fixes

* **HTTPTransport:** isConnected for node.js env ([37eeef0](https://bitbucket.org/thermsio/rpc-client-ts/commits/37eeef012cea649a5f202c21595264dcdaebf115))

## [3.0.1](http://bitbucket.org/thermsio/rpc-client-ts/compare/v3.0.0...v3.0.1) (2024-03-22)


### Bug Fixes

* CallResponseError message include request shorthand string ([fd5e862](http://bitbucket.org/thermsio/rpc-client-ts/commits/fd5e862472699d0b4535274c72eeb562b828f0d7))

# [3.0.0](http://bitbucket.org/thermsio/rpc-client-ts/compare/v2.19.3...v3.0.0) (2024-03-21)


### Code Refactoring

* throw a CallResponseError that extends Error ([b4d9eda](http://bitbucket.org/thermsio/rpc-client-ts/commits/b4d9eda9b1ac23f4e3d4c48967eca72cbccf141a))


### BREAKING CHANGES

* when RPC response success=[secure] an Error object is thrown

## [2.19.3](https://bitbucket.org/thermsio/rpc-client-ts/compare/v2.19.2...v2.19.3) (2024-02-23)


### Bug Fixes

* onTransportRequestError add transport type ([5ebfc5a](https://bitbucket.org/thermsio/rpc-client-ts/commits/5ebfc5add58426e59d38f22b57e0b198b7371b50))

## [2.19.2](http://bitbucket.org/thermsio/rpc-client-ts/compare/v2.19.1...v2.19.2) (2024-02-23)


### Bug Fixes

* onTransportRequestError add transport type ([1cd49c8](http://bitbucket.org/thermsio/rpc-client-ts/commits/1cd49c83e308161cbeafc4ba9f511a322fc813a1))

## [2.19.1](https://bitbucket.org/thermsio/rpc-client-ts/compare/v2.19.0...v2.19.1) (2024-02-21)


### Bug Fixes

* onTransportRequestError RPCClientOptions ([d5d2834](https://bitbucket.org/thermsio/rpc-client-ts/commits/d5d283455d7929d6a32c95d97dfd04fc630d5ea7))

# [2.19.0](http://bitbucket.org/thermsio/rpc-client-ts/compare/v2.18.2...v2.19.0) (2024-02-21)


### Features

* add onTransportRequestError optional callback ([3f032f9](http://bitbucket.org/thermsio/rpc-client-ts/commits/3f032f9700b66640cf8a0df2ae3e0d093b3a8c5f))

## [2.18.2](http://bitbucket.org/thermsio/rpc-client-ts/compare/v2.18.1...v2.18.2) (2024-02-16)


### Bug Fixes

* update deps ([123065d](http://bitbucket.org/thermsio/rpc-client-ts/commits/123065db901e6212985e7e33ca65525001f9b90f))

## [2.18.1](http://bitbucket.org/thermsio/rpc-client-ts/compare/v2.18.0...v2.18.1) (2023-11-30)


### Bug Fixes

* update deps ([e194ff8](http://bitbucket.org/thermsio/rpc-client-ts/commits/e194ff893e3d5b729cf90d65f29308682006acad))

# [2.18.0](http://bitbucket.org/thermsio/rpc-client-ts/compare/v2.17.0...v2.18.0) (2023-11-08)


### Features

* export CallOptions ([372b7f3](http://bitbucket.org/thermsio/rpc-client-ts/commits/372b7f3f456390cd3a574396f8c6e86df493acaf))

# [2.17.0](http://bitbucket.org/thermsio/rpc-client-ts/compare/v2.16.7...v2.17.0) (2023-11-08)


### Features

* allow specific transport to be used in RPC call ([7349b2a](http://bitbucket.org/thermsio/rpc-client-ts/commits/7349b2ac554372f9ba14acdcbf570e6b24df2f66))

## [2.16.7](http://bitbucket.org/thermsio/rpc-client-ts/compare/v2.16.6...v2.16.7) (2023-10-29)


### Bug Fixes

* update major version deps ([85fa69b](http://bitbucket.org/thermsio/rpc-client-ts/commits/85fa69bb37e4be54a9d2a931dada164b0ffa0aa8))

## [2.16.6](http://bitbucket.org/thermsio/rpc-client-ts/compare/v2.16.5...v2.16.6) (2023-09-12)


### Bug Fixes

* rollup use v3 terser ([db86893](http://bitbucket.org/thermsio/rpc-client-ts/commits/db86893a74613eb47093c43acbadd0da8a4af3eb))
* update deps ([67f988f](http://bitbucket.org/thermsio/rpc-client-ts/commits/67f988f70550568d7d082edea03bf83d7f37cd8a))

## [2.16.5](http://bitbucket.org/thermsio/rpc-client-ts/compare/v2.16.4...v2.16.5) (2023-08-30)


### Bug Fixes

* update deps ([9549694](http://bitbucket.org/thermsio/rpc-client-ts/commits/95496947016714f90cd4d665c3291091ce9d3e3b))

## [2.16.4](http://bitbucket.org/thermsio/rpc-client-ts/compare/v2.16.3...v2.16.4) (2023-08-04)


### Bug Fixes

* update deps ([b129ae2](http://bitbucket.org/thermsio/rpc-client-ts/commits/b129ae20d717a17db9bbf2e461bec6fb43c49e44))

## [2.16.3](http://bitbucket.org/thermsio/rpc-client-ts/compare/v2.16.2...v2.16.3) (2023-07-05)


### Bug Fixes

* update deps ([fda7c2e](http://bitbucket.org/thermsio/rpc-client-ts/commits/fda7c2ebc8577dbba5581a79073ce06ae4b11a25))

## [2.16.2](http://bitbucket.org/thermsio/rpc-client-ts/compare/v2.16.1...v2.16.2) (2023-06-27)


### Bug Fixes

* update deps ([b22c163](http://bitbucket.org/thermsio/rpc-client-ts/commits/b22c16334c112c73ec431a52f2dc3cdd109e0539))

## [2.16.1](http://bitbucket.org/thermsio/rpc-client-ts/compare/v2.16.0...v2.16.1) (2023-05-02)


### Bug Fixes

* chore update deps ([2ac38b4](http://bitbucket.org/thermsio/rpc-client-ts/commits/2ac38b48b005f76dbad58861df93e3f9b909ac69))

# [2.16.0](https://bitbucket.org/thermsio/rpc-client-ts/compare/v2.15.0...v2.16.0) (2023-04-19)


### Features

* PromiseWrapper description ([5f92958](https://bitbucket.org/thermsio/rpc-client-ts/commits/5f92958212ea251e766662508d800c8a368cb50a))
* refactor and fix per call timeout ([6993f8c](https://bitbucket.org/thermsio/rpc-client-ts/commits/6993f8cbd10e1c316045f280d2e3aadd1e095c29))

# [2.15.0](http://bitbucket.org/thermsio/rpc-client-ts/compare/v2.14.0...v2.15.0) (2023-02-27)


### Features

* add console logging when transports fail ([be904fd](http://bitbucket.org/thermsio/rpc-client-ts/commits/be904fdd6810a6390d89c3b38df053ed0034b67a))

# [2.14.0](http://bitbucket.org/thermsio/rpc-client-ts/compare/v2.13.1...v2.14.0) (2023-02-27)


### Features

* add RPC request opts for timeout ([fcd3c70](http://bitbucket.org/thermsio/rpc-client-ts/commits/fcd3c70ef0cf897df551ffadd9807b506424ccf6))

## [2.13.1](http://bitbucket.org/thermsio/rpc-client-ts/compare/v2.13.0...v2.13.1) (2023-01-29)


### Bug Fixes

* update deps ([fcca878](http://bitbucket.org/thermsio/rpc-client-ts/commits/fcca878c3ea3f612ab151635dab69a1d37461f26))

# [2.13.0](http://bitbucket.org/thermsio/rpc-client-ts/compare/v2.12.0...v2.13.0) (2022-11-19)


### Features

* update deps ([1f31781](http://bitbucket.org/thermsio/rpc-client-ts/commits/1f31781afcfb50ccca6e98f72e8caccf9627a3b8))

# [2.12.0](http://bitbucket.org/thermsio/rpc-client-ts/compare/v2.11.4...v2.12.0) (2022-11-03)


### Features

* add unsubscribe callbacks ([2963eb4](http://bitbucket.org/thermsio/rpc-client-ts/commits/2963eb43be7aaafd6230383b1aee5d7d56a2a96f))

## [2.11.4](https://bitbucket.org/thermsio/rpc-client-ts/compare/v2.11.3...v2.11.4) (2022-10-19)


### Bug Fixes

* types ([91e57fe](https://bitbucket.org/thermsio/rpc-client-ts/commits/91e57febe4ef6c1aed7b4b76f7923b052e26fc7f))

## [2.11.3](https://bitbucket.org/thermsio/rpc-client-ts/compare/v2.11.2...v2.11.3) (2022-10-19)


### Bug Fixes

* update minor deps ([b2c291a](https://bitbucket.org/thermsio/rpc-client-ts/commits/b2c291aa5f94a1f4841b42ddbd1540cb210408d5))

## [2.11.2](http://bitbucket.org/thermsio/rpc-client-ts/compare/v2.11.1...v2.11.2) (2022-08-31)


### Bug Fixes

* RPCClient.transports() is now a function ([8a544ac](http://bitbucket.org/thermsio/rpc-client-ts/commits/8a544ac3338475b9f4cde5157d7fc939fe713ccd))

## [2.11.1](https://bitbucket.org/thermsio/rpc-client-ts/compare/v2.11.0...v2.11.1) (2022-08-31)


### Bug Fixes

* exports package.json ([79dd64c](https://bitbucket.org/thermsio/rpc-client-ts/commits/79dd64c226d511b602a7e50cda419679409d2a94))

# [2.11.0](https://bitbucket.org/thermsio/rpc-client-ts/compare/v2.10.5...v2.11.0) (2022-08-31)


### Features

* expose transports on RPCClient interface ([25e675d](https://bitbucket.org/thermsio/rpc-client-ts/commits/25e675d06fdbaa67238ddde1c6a32f5996b5c5ec))

## [2.10.5](https://bitbucket.org/thermsio/rpc-client-ts/compare/v2.10.4...v2.10.5) (2022-08-30)


### Bug Fixes

* no sourcemap for umd build ([117ffc5](https://bitbucket.org/thermsio/rpc-client-ts/commits/117ffc51f3e5000686c515565c9f83b8726de24e))

## [2.10.4](https://bitbucket.org/thermsio/rpc-client-ts/compare/v2.10.3...v2.10.4) (2022-08-29)


### Bug Fixes

* npm publish files include src ([fff26f0](https://bitbucket.org/thermsio/rpc-client-ts/commits/fff26f0fdf9ce4d908da039aed133c46afc12060))

## [2.10.3](https://bitbucket.org/thermsio/rpc-client-ts/compare/v2.10.2...v2.10.3) (2022-08-29)


### Bug Fixes

* force bump version ([a1158c2](https://bitbucket.org/thermsio/rpc-client-ts/commits/a1158c27722285332a55c376d8605c1906d300c2))

## [2.10.1](https://bitbucket.org/thermsio/rpc-client-ts/compare/v2.10.0...v2.10.1) (2022-08-29)


### Bug Fixes

* force bump version ([bfdde82](https://bitbucket.org/thermsio/rpc-client-ts/commits/bfdde82dd646bd499ad47f9d84da2ee996a7560c))

# [2.10.0](http://bitbucket.org/thermsio/rpc-client-ts/compare/v2.9.1...v2.10.0) (2022-08-02)


### Features

* add timestamp to WS connected/disconnected logging ([c3f316d](http://bitbucket.org/thermsio/rpc-client-ts/commits/c3f316d5afdc79c1761ae9bd7cfa72a761c4d82f))

## [2.9.1](http://bitbucket.org/thermsio/rpc-client-ts/compare/v2.9.0...v2.9.1) (2022-08-02)


### Bug Fixes

* lru cache clear call ([dd9d0ea](http://bitbucket.org/thermsio/rpc-client-ts/commits/dd9d0eadcab26481b810761974a24355e6c77418))

# [2.9.0](http://bitbucket.org/thermsio/rpc-client-ts/compare/v2.8.1...v2.9.0) (2022-07-01)


### Features

* update deps ([b723d34](http://bitbucket.org/thermsio/rpc-client-ts/commits/b723d348130cea7d5125da3789db5423a906b3fd))

## [2.8.1](https://bitbucket.org/thermsio/rpc-client-ts/compare/v2.8.0...v2.8.1) (2022-06-14)


### Bug Fixes

* sendClientMessageToServer() structure ([00d05f4](https://bitbucket.org/thermsio/rpc-client-ts/commits/00d05f49cc67f850fba4684185084732f5b3431c))

# [2.8.0](https://bitbucket.org/thermsio/rpc-client-ts/compare/v2.7.0...v2.8.0) (2022-06-14)


### Features

* add subscribeToServerMessages & unsubscribeFromServerMessages methods to RPCClient ([7645f6a](https://bitbucket.org/thermsio/rpc-client-ts/commits/7645f6a85e45f8e626c79fd65beb651e86e6bb26))

# [2.7.0](https://bitbucket.org/thermsio/rpc-client-ts/compare/v2.6.0...v2.7.0) (2022-06-14)


### Features

* add sendClientMessageToServer() for sending clientMessage's to RPC-server ([98a3fb4](https://bitbucket.org/thermsio/rpc-client-ts/commits/98a3fb4ecca3251a5f424532aded09129f3a7619))

# [2.6.0](https://bitbucket.org/thermsio/rpc-client-ts/compare/v2.5.4...v2.6.0) (2022-06-11)


### Features

* add RPCClient.setIdentityMetadata() ([2f083f4](https://bitbucket.org/thermsio/rpc-client-ts/commits/2f083f46d063d201f326ab95f06ff20610e5e4f6))
* update deps ([782c0c0](https://bitbucket.org/thermsio/rpc-client-ts/commits/782c0c00a3223d29e7bda3f135917009bc28994d))

## [2.5.4](https://bitbucket.org/thermsio/rpc-client-ts/compare/v2.5.3...v2.5.4) (2022-05-22)


### Bug Fixes

* WebSocketTransport close connection code 1000 ([5435c0d](https://bitbucket.org/thermsio/rpc-client-ts/commits/5435c0d80ba0d24b2a148ddc4534ea59ae76a5ce))

## [2.5.3](https://bitbucket.org/thermsio/rpc-client-ts/compare/v2.5.2...v2.5.3) (2022-05-22)


### Bug Fixes

* websocket transport reset wait for 100ms before reconnecting ([1f1b838](https://bitbucket.org/thermsio/rpc-client-ts/commits/1f1b8381c5cf0df9a887ad715f5d93ce65bf13e4))

## [2.5.2](https://bitbucket.org/thermsio/rpc-client-ts/compare/v2.5.1...v2.5.2) (2022-05-22)


### Bug Fixes

* WebSocketTransport reset conection and setting Identity logic ([ac816d7](https://bitbucket.org/thermsio/rpc-client-ts/commits/ac816d7cac703d0153c289cc97c90f68909615d7))

## [2.5.1](http://bitbucket.org/thermsio/rpc-client-ts/compare/v2.5.0...v2.5.1) (2022-04-20)


### Bug Fixes

* InMemoryCache.ts ttl ([16d4591](http://bitbucket.org/thermsio/rpc-client-ts/commits/16d459189478a4e08231a2c5a1909aa09e0fa872))

# [2.5.0](http://bitbucket.org/thermsio/rpc-client-ts/compare/v2.4.1...v2.5.0) (2022-04-13)


### Features

* registerWebSocketConnectionStatusChangeListener ([bd4fd03](http://bitbucket.org/thermsio/rpc-client-ts/commits/bd4fd03919bbe346f21c698d48b72437a1755f06))

## [2.4.1](http://bitbucket.org/thermsio/rpc-client-ts/compare/v2.4.0...v2.4.1) (2022-04-12)


### Bug Fixes

* chore update deps ([5083595](http://bitbucket.org/thermsio/rpc-client-ts/commits/50835953b2661b423770f78b67bb204b464bbf60))

# [2.4.0](http://bitbucket.org/thermsio/rpc-client-ts/compare/v2.3.1...v2.4.0) (2022-03-25)


### Features

* subscribe to RPC responses events ([20f4bee](http://bitbucket.org/thermsio/rpc-client-ts/commits/20f4beec499f39f212b5a842eb582b4367b827f3))

## [2.3.1](http://bitbucket.org/thermsio/rpc-client-ts/compare/v2.3.0...v2.3.1) (2022-03-14)


### Bug Fixes

* docs for UMD build ([473bf1d](http://bitbucket.org/thermsio/rpc-client-ts/commits/473bf1d2668c30220f2dfefa678c6074fbfe1b2d))

# [2.3.0](http://bitbucket.org/thermsio/rpc-client-ts/compare/v2.2.3...v2.3.0) (2022-03-14)


### Features

* add UMD bundle to build ([b921eb2](http://bitbucket.org/thermsio/rpc-client-ts/commits/b921eb283cca39404601a5d2db079f998893135a))

## [2.2.3](http://bitbucket.org/thermsio/rpc-client-ts/compare/v2.2.2...v2.2.3) (2022-01-19)


### Bug Fixes

* build lib ES2015 ([8401ed7](http://bitbucket.org/thermsio/rpc-client-ts/commits/8401ed7663b5cf7a2a3a3e9c9a0bc5019765e7d2))

## [2.2.2](http://bitbucket.org/thermsio/rpc-client-ts/compare/v2.2.1...v2.2.2) (2022-01-06)


### Bug Fixes

* update deps and tsconfig sourcemaps ([7b30bf9](http://bitbucket.org/thermsio/rpc-client-ts/commits/7b30bf93769d73223c80f30c9520ed50397ec705))

## [2.2.1](http://bitbucket.org/thermsio/rpc-client-ts/compare/v2.2.0...v2.2.1) (2021-12-06)


### Bug Fixes

* add debug logs to WebSocketTransport ([9954900](http://bitbucket.org/thermsio/rpc-client-ts/commits/9954900bd40fd63cf9bcd1108de504af640515be))

# [2.2.0](http://bitbucket.org/thermsio/rpc-client-ts/compare/v2.1.2...v2.2.0) (2021-10-02)


### Features

* added registerResponseInterceptor registerRequestInterceptor for adding additional interceptors after initialization ([72ac5be](http://bitbucket.org/thermsio/rpc-client-ts/commits/72ac5bec8a726be5503c8e089d855c88aa91244a))

## [2.1.2](http://bitbucket.org/thermsio/rpc-client-ts/compare/v2.1.1...v2.1.2) (2021-10-01)


### Bug Fixes

* persist identity on calls and override identity on single calls ([05e8394](http://bitbucket.org/thermsio/rpc-client-ts/commits/05e839448d4988235a85bb75d1962ab8e4ac74cc))

## [2.1.1](http://bitbucket.org/thermsio/rpc-client-ts/compare/v2.1.0...v2.1.1) (2021-09-22)


### Bug Fixes

* return  not optional ([7b2553a](http://bitbucket.org/thermsio/rpc-client-ts/commits/7b2553a8e9b650834a72190db9f2097a4dd98faf))

# [2.1.0](http://bitbucket.org/thermsio/rpc-client-ts/compare/v2.0.0...v2.1.0) (2021-09-19)


### Features

* add Args/Data types for call() and getCallCache() methods ([c602066](http://bitbucket.org/thermsio/rpc-client-ts/commits/c602066d8517e2882ab737d1bdfe2851ef621290))

# [2.0.0](http://bitbucket.org/thermsio/rpc-client-ts/compare/v1.2.4...v2.0.0) (2021-09-18)


### Code Refactoring

* export types ([fd30709](http://bitbucket.org/thermsio/rpc-client-ts/commits/fd30709aa040beb050e30298352be979e98e18a1))


### BREAKING CHANGES

* no longer a UMD build in npm package

## [1.2.4](http://bitbucket.org/thermsio/rpc-client-ts/compare/v1.2.3...v1.2.4) (2021-05-19)


### Bug Fixes

* update deps ([b6c5bea](http://bitbucket.org/thermsio/rpc-client-ts/commits/b6c5beac403e11e7567838f58ecf86ebbd992722))

## [1.2.3](http://bitbucket.org/thermsio/rpc-client-ts/compare/v1.2.2...v1.2.3) (2021-05-18)


### Bug Fixes

* shorthand procedure string scope::procedure::version ([95670d0](http://bitbucket.org/thermsio/rpc-client-ts/commits/95670d047ca1d9306ca0dc7156215df051122e2d))

## [1.2.2](http://bitbucket.org/thermsio/rpc-client-ts/compare/v1.2.1...v1.2.2) (2021-05-17)


### Bug Fixes

* debug logging ([af0ce23](http://bitbucket.org/thermsio/rpc-client-ts/commits/af0ce2395f6e9c27f4c1653cb58fbe1129e1f0aa))
* postinstall script ([429b495](http://bitbucket.org/thermsio/rpc-client-ts/commits/429b49563acdd7e958cf7de2fcb1d36c2216f89a))
* **InMemoryCache:** json string/parse before get/set ([b7cb925](http://bitbucket.org/thermsio/rpc-client-ts/commits/b7cb925633873db34c874a910911ba23f738c057))

## [1.2.1](http://bitbucket.org/thermsio/rpc-client-ts/compare/v1.2.0...v1.2.1) (2021-05-16)


### Bug Fixes

* **WebSocketTransport:** delete pending request promises after finished ([ebe3cef](http://bitbucket.org/thermsio/rpc-client-ts/commits/ebe3cef1440a4e1ca42b7321fede6e888f02b710))

# [1.2.0](http://bitbucket.org/thermsio/rpc-client-ts/compare/v1.1.0...v1.2.0) (2021-05-10)


### Features

* expose websocket connetion status changes on RPCClient ([67f4d86](http://bitbucket.org/thermsio/rpc-client-ts/commits/67f4d86b5437eaf5aa90835bd6bfff81fdec3a68))

# [1.1.0](http://bitbucket.org/thermsio/rpc-client-ts/compare/v1.0.13...v1.1.0) (2021-05-04)


### Features

* **RPCClient:** call will throw when success == false ([6c29f94](http://bitbucket.org/thermsio/rpc-client-ts/commits/6c29f9420f8e2e9d06b9b4671cc8c002f518caac))

## [1.0.13](http://bitbucket.org/thermsio/rpc-client-ts/compare/v1.0.12...v1.0.13) (2021-04-27)


### Bug Fixes

* **WebSocket:** resetConnection logic ([0b51ab3](http://bitbucket.org/thermsio/rpc-client-ts/commits/0b51ab3916dea074d7673d1c2190ca77a214fb4d))

## [1.0.12](http://bitbucket.org/thermsio/rpc-client-ts/compare/v1.0.11...v1.0.12) (2021-04-27)


### Bug Fixes

* websocket set identity logic ([8a8bb01](http://bitbucket.org/thermsio/rpc-client-ts/commits/8a8bb0174619582cc6b21289634785a48557044e))

## [1.0.11](http://bitbucket.org/thermsio/rpc-client-ts/compare/v1.0.10...v1.0.11) (2021-04-26)


### Bug Fixes

* debug logs in CallManager ([75d8ac8](http://bitbucket.org/thermsio/rpc-client-ts/commits/75d8ac88bf08f2b2a4eecfa80cb1ca63b546503f))

## [1.0.10](http://bitbucket.org/thermsio/rpc-client-ts/compare/v1.0.9...v1.0.10) (2021-04-26)


### Bug Fixes

* bundle with node-polyfills for debug pkg ([04a85b0](http://bitbucket.org/thermsio/rpc-client-ts/commits/04a85b09f22126131b12b1a306773792d18d86eb))

## [1.0.9](http://bitbucket.org/thermsio/rpc-client-ts/compare/v1.0.8...v1.0.9) (2021-04-26)


### Bug Fixes

* add debug logs ([a01bfdd](http://bitbucket.org/thermsio/rpc-client-ts/commits/a01bfddcbc018df75463306ffc54dd167cb46f4d))
* setIdentity and add tests ([4e68082](http://bitbucket.org/thermsio/rpc-client-ts/commits/4e68082bc0ef0ff76373590c684026b0d02496cd))

## [1.0.8](http://bitbucket.org/thermsio/rpc-client-ts/compare/v1.0.7...v1.0.8) (2021-04-26)


### Bug Fixes

* **WebSocketTransport:** set identity fixes ([7a545e9](http://bitbucket.org/thermsio/rpc-client-ts/commits/7a545e9ac8dcb6f7f0506d2df9e12116d210946b))

## [1.0.7](http://bitbucket.org/thermsio/rpc-client-ts/compare/v1.0.6...v1.0.7) (2021-04-26)


### Bug Fixes

* update deps ([de32df7](http://bitbucket.org/thermsio/rpc-client-ts/commits/de32df75c2b5050e37a79259a166574ebee04d12))

## [1.0.6](http://bitbucket.org/thermsio/rpc-client-ts/compare/v1.0.5...v1.0.6) (2021-04-26)


### Bug Fixes

* remove atomic-sleep ([cbeea27](http://bitbucket.org/thermsio/rpc-client-ts/commits/cbeea27761278c66449708be1672142fb3b1b283))

## [1.0.5](http://bitbucket.org/thermsio/rpc-client-ts/compare/v1.0.4...v1.0.5) (2021-04-26)


### Bug Fixes

* remove atomic-sleep ([c20972c](http://bitbucket.org/thermsio/rpc-client-ts/commits/c20972c99c305f890de357e4b22f241b3cb7508e))

## [1.0.4](http://bitbucket.org/thermsio/rpc-client-ts/compare/v1.0.3...v1.0.4) (2021-04-26)


### Bug Fixes

* rename type files removing .d.ts ([2f4b4d9](http://bitbucket.org/thermsio/rpc-client-ts/commits/2f4b4d9786a310ced8fe024d781731c0a646ed76))

## [1.0.3](http://bitbucket.org/thermsio/rpc-client-ts/compare/v1.0.2...v1.0.3) (2021-04-01)


### Bug Fixes

* CallManager transport delegation while loop ([7c66018](http://bitbucket.org/thermsio/rpc-client-ts/commits/7c6601840df2dfa7a1429737c1ea8f351f5b1168))

## [1.0.2](http://bitbucket.org/thermsio/rpc-client-ts/compare/v1.0.1...v1.0.2) (2021-03-24)


### Bug Fixes

* npmignore ([88b23b7](http://bitbucket.org/thermsio/rpc-client-ts/commits/88b23b7d21bda37a2a7a10c51d3443460378b6a0))

## [1.0.1](http://bitbucket.org/thermsio/rpc-client-ts/compare/v1.0.0...v1.0.1) (2021-03-24)


### Bug Fixes

* build script ([707cc90](http://bitbucket.org/thermsio/rpc-client-ts/commits/707cc90615db0123f2f2492632e67fd3b8379834))

# 1.0.0 (2021-03-22)


### Bug Fixes

* build ([d36df8a](http://bitbucket.org/thermsio/rpc-client-ts/commits/d36df8ac602774a4ed3857c6ad8ba416db5226bd))
* CallResponseDTO typo ([6a8ee01](http://bitbucket.org/thermsio/rpc-client-ts/commits/6a8ee0176ad5af1762dfe090945d8728e4d5489e))
* deps for build ([498e081](http://bitbucket.org/thermsio/rpc-client-ts/commits/498e0810cb5a39f413e410f085e5aad8a24a9762))
* jest tests and tsconfig changed ([dfd951b](http://bitbucket.org/thermsio/rpc-client-ts/commits/dfd951bd39cf5f613bf5b4d7cd38dd3d107bae11))
* request and response interceptors ([29b60d3](http://bitbucket.org/thermsio/rpc-client-ts/commits/29b60d32c9de21ad8333a6fd78d787d83bf0241d))
* tests ([63efa54](http://bitbucket.org/thermsio/rpc-client-ts/commits/63efa54ecf6cbe62331164d06843cd4156c6f778))


### Features

* basic server w/ http and tests working ([caeddc8](http://bitbucket.org/thermsio/rpc-client-ts/commits/caeddc8f3ebe797c436cab70c9657b5b5e979129))
* HTTPTransport logic with fetch ([002692d](http://bitbucket.org/thermsio/rpc-client-ts/commits/002692d376264278b3dd12c03f76dbcf2589217d))
* setIdentity RPCClient ([7757fe6](http://bitbucket.org/thermsio/rpc-client-ts/commits/7757fe650e65988a8fc92ed42dbaccd0e7c74980))
* **Client:** ProcedureCall methods implemented w/ passing tests ([04887d2](http://bitbucket.org/thermsio/rpc-client-ts/commits/04887d2bcf7a62345771c3b509ee78a98c318c3f))
