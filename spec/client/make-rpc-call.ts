import { CallResponseError, RPCClient } from '../../src'
import { MockHTTPTransport } from './test-utils/MockHTTPTransport'
import { CallRequestDTO } from '../../src/CallRequestDTO'
import { CallResponseDTO } from '../../src/CallResponseDTO'
import { MockWsTransport } from './test-utils/MockWsTransport'
import { TransportType } from '../../src/transports/Transport'
import { RPCClientIdentity } from '@therms/rpc-server'
import { CallOptions } from '../../src/RPCClient'

describe('make rpc calls', () => {
  const mockCallArgs = {
    email: 'cory@therms.io',
    password: '123456',
  }

  const rpc = new RPCClient({
    cacheMaxAgeMs: 100,
    deadlineMs: 100,
    transports: {
      http: new MockHTTPTransport(),
    },
  })

  it('can make a procedure call', async () => {
    const { code, data, success } = await rpc.call({
      args: mockCallArgs,
      procedure: 'get-some-data',
      scope: 'some-data',
      version: '1',
    })

    expect(data).toEqual(mockCallArgs)
    expect(code).toEqual(200)
    expect(success).toEqual(true)
  })

  it('throws a CallResponse when RPC is unsuccesful', async () => {
    try {
      const { code, data, success } = await rpc.call({
        args: { fail: true },
        procedure: 'fail',
        scope: 'anything',
        version: '1',
      })
    } catch (e: any) {
      // we don't want typeof to be 'function' which is a Proxy
      expect(typeof e).toEqual('object')
      expect(e.response.code).toEqual(422)
      expect(e.response.success).toBeFalsy()
    }
  })

  // todo: to be implemented...
  // it("subscribes and received remote call updates", async (done) =pendingPromisesForResponse[callResponseDTO.correlationId]> {
  //   getSomeDataProcedure.onRemoteCallUpdate((response) => {
  //     // do somethig with the new call response data...
  //
  //     getSomeDataProcedure.cancelCallResponseUpdates();
  //
  //     // expect(getSomeDataProcedure.isSubscribedToRemoteUpdates).toBeFalsy();
  //
  //     done();
  //   });
  //
  //   expect(getSomeDataProcedure.isSubscribedToRemoteUpdates).toBeTruthy();
  // });

  it('caches responses', async () => {
    const callOptions = {
      args: mockCallArgs,
      procedure: 'get-cached-data',
      scope: 'cache',
      version: '1',
    }

    const cachedResponse1 = rpc.getCallCache(callOptions)

    expect(cachedResponse1).toBeUndefined()

    await rpc.call(callOptions)

    const cachedResponse2 = rpc.getCallCache(callOptions)

    expect(cachedResponse2).toBeDefined()
    expect(cachedResponse2?.code).toEqual(200)
    expect(cachedResponse2?.data).toEqual(mockCallArgs)
    expect(cachedResponse2?.success).toEqual(true)
  })

  it('clears specific request cache', async () => {
    const callOptions = {
      args: mockCallArgs,
      procedure: 'clear-cached-data-request',
      scope: 'cache',
      version: '1',
    }

    await rpc.call(callOptions)

    const cachedResponse1 = rpc.getCallCache(callOptions)

    expect(cachedResponse1).toBeDefined()

    rpc.clearCache(callOptions)

    const cachedResponse2 = rpc.getCallCache(callOptions)

    expect(cachedResponse2).toBeUndefined()
  })

  it('clears entire cache', async () => {
    const callOptions = {
      args: mockCallArgs,
      procedure: 'clear-cached-data',
      scope: 'cache',
      version: '1',
    }

    await rpc.call(callOptions)

    const cachedResponse1 = rpc.getCallCache(callOptions)

    expect(cachedResponse1).toBeDefined()

    rpc.clearCache()

    const cachedResponse2 = rpc.getCallCache(callOptions)

    expect(cachedResponse2).toBeUndefined()
  })

  it('auto clears cache after expire', async () => {
    const callOptions = {
      args: mockCallArgs,
      procedure: 'clear-cached-data-expire',
      scope: 'cache',
      version: '1',
    }

    await rpc.call(callOptions)

    const cachedResponse1 = rpc.getCallCache(callOptions)

    expect(cachedResponse1).toBeDefined()

    return new Promise((done) => {
      setTimeout(() => {
        const cachedResponse2 = rpc.getCallCache(callOptions)

        expect(cachedResponse2).toBeUndefined()

        done(true)
      }, 150)
    })
  })

  it('enforces a call deadline by throwing', async () => {
    const callOptions = {
      args: { delayCallForTesting: 150 },
      procedure: 'call-deadline',
      scope: 'cache',
      version: '1',
    }

    await expect(() => rpc.call(callOptions)).rejects.toEqual(expect.any(Error))
  })

  it('handles duplicate in-flight calls', async () => {
    const callOptions = {
      args: { delayCallForTesting: 25 },
      procedure: 'duplicate-call',
      scope: 'inflight',
      version: '1',
    }

    rpc.call(callOptions)
    rpc.call(callOptions)

    return new Promise((done) => {
      // we have to wait because a Call is async and we can't get the in-flight count immediately
      setTimeout(() => {
        expect(rpc.getInFlightCallCount()).toEqual(1)

        done(true)
      }, 10)
    })
  })

  it('intercepts requests', async () => {
    const rpc = new RPCClient({
      cacheMaxAgeMs: 100,
      deadlineMs: 100,
      requestInterceptor: (request: CallRequestDTO) => {
        request.args.count = request.args.count + 1

        return request
      },
      transports: {
        http: new MockHTTPTransport(),
      },
    })

    const callOptions = {
      args: { count: 1 },
      procedure: 'intercept-request',
      scope: 'intercept',
      version: '1',
    }

    const { data } = await rpc.call(callOptions)

    expect(data.count).toEqual(2)
  })

  it('intercepts responses', async () => {
    const rpc = new RPCClient({
      cacheMaxAgeMs: 100,
      deadlineMs: 100,
      responseInterceptor: (
        response: CallResponseDTO,
        request: CallRequestDTO,
      ) => {
        response.data.count = response.data.count / 2 + request.args.count

        return response
      },
      transports: {
        http: new MockHTTPTransport(),
      },
    })

    const callOptions = {
      args: { count: 10 },
      procedure: 'intercept-response',
      scope: 'intercept',
      version: '1',
    }

    const { data } = await rpc.call(callOptions)

    expect(data.count).toEqual(15)
  })

  it('allows a timeout override per call()', async () => {
    const rpc = new RPCClient({
      deadlineMs: 200,
      transports: {
        http: new MockHTTPTransport(),
      },
    })

    const request = {
      procedure: 'timeout',
      scope: 'test',
      version: '1',
    }

    await expect(() =>
      rpc.call(request, { delayCallForTesting: 250 }),
    ).rejects.toEqual(expect.any(Error))

    await expect(() =>
      rpc.call(request, { delayCallForTesting: 800 }, { timeout: 750 }),
    ).rejects.toEqual(expect.any(Error))

    const res = await rpc.call(
      request,
      { delayCallForTesting: 700 },
      { timeout: 750 },
    )

    expect(res.success).toBeTruthy()
  })

  it('allows specifying the transport to use per call()', async () => {
    const requestInterceptor = async (request: CallRequestDTO) => {
      if (request.args?.fail) {
        return new CallResponseDTO({
          code: 500,
          data: undefined,
          success: false,
        })
      }

      if (request.args?.wait) {
        return new Promise<CallResponseDTO>((resolve) => {
          setTimeout(() => {
            resolve(
              new CallResponseDTO({
                code: 200,
                data: undefined,
                success: true,
              }),
            )
          }, request.args.wait)
        })
      }

      return new CallResponseDTO({
        code: 200,
        data: undefined,
        success: true,
      })
    }

    const http = new MockHTTPTransport({
      requestInterceptor,
    })
    const websocket = new MockWsTransport({
      requestInterceptor,
    })

    jest.spyOn(http, 'sendRequest')
    jest.spyOn(websocket, 'sendRequest')

    const rpc = new RPCClient({
      deadlineMs: 200,
      transports: {
        http,
        websocket,
      },
    })

    const request = {
      procedure: 'specify-transport',
      scope: 'test',
      version: '1',
    }

    const rejectedResponseError = (await rpc
      .call(
        request,
        {
          fail: true,
        },
        {
          transport: 'http',
        },
      )
      .catch((e) => e)) as CallResponseError

    expect(rejectedResponseError).toEqual(expect.any(Error))
    expect(rejectedResponseError.response).toBeTruthy()
    expect(rejectedResponseError.response.code).toEqual(500)
    expect(rejectedResponseError.response.success).toBeFalsy()

    await expect(() =>
      rpc.call(
        request,
        {
          wait: 300, // wait longer that the RPCClient#deadlineMs
        },
        {
          transport: 'http',
        },
      ),
    ).rejects.toEqual(expect.any(Error))

    expect(http.sendRequest).toHaveBeenCalledTimes(2)
    expect(websocket.sendRequest).toHaveBeenCalledTimes(0)
  })

  it('should use the preffered transport if specified', async () => {
    const rpc = new RPCClient({
      transports: {
        http: new MockHTTPTransport({
          responseInterceptor: async (response, request) => {
            return new CallResponseDTO({
              ...response,
              data: { http: 'http' },
            })
          },
        }),
        websocket: new MockWsTransport({
          responseInterceptor: async (response, request) => {
            return new CallResponseDTO({
              ...response,
              data: { ws: 'ws' },
            })
          },
        }),
      },
      transportOptions: {
        preferredTransport: TransportType.http,
      },
    })

    const resp1 = await rpc.call('test::any-call', {})
    const resp2 = await rpc.call(
      'test::any-call',
      {},
      { transport: 'websocket' },
    )

    expect(resp1.data.http).toEqual('http')
    expect(resp2.data.ws).toEqual('ws')

    const resp3 = await rpc.call('test::any-call', {})

    expect(resp3.data.http).toEqual('http')
  })
})
