import { RPCClient } from '../../src'
import { MockHTTPTransport } from './test-utils/MockHTTPTransport'

describe('subscribe to RPC responses', () => {
  const rpc = new RPCClient({
    transports: {
      http: new MockHTTPTransport(),
    },
  })

  it('subscribes to RPCs and a call when a response occurs', async () => {
    const handler = jest.fn((response) => response)

    const filter = {
      args: { args: 'abc' },
      request: { scope: 'test', procedure: 'procedure1', version: '1' },
    }

    rpc.subscribe(filter, handler)

    await rpc.call(filter.request, { args: 'abc' })
    await rpc.call(filter.request, { args: 'abc' })

    expect(handler).toHaveBeenCalled()
    expect(handler).toHaveBeenCalledTimes(2)
    expect(handler).toHaveBeenLastCalledWith({ args: 'abc' })
  })

  it('subscribes to RPCs w/ no args', async () => {
    const handler = jest.fn((response) => response)

    const filter = {
      request: { scope: 'test', procedure: 'procedure1', version: '1' },
    }

    rpc.subscribe(filter, handler)

    await rpc.call(filter.request)
    await rpc.call(filter.request)
    await rpc.call(filter.request)

    expect(handler).toHaveBeenCalled()
    expect(handler).toHaveBeenCalledTimes(3)
    expect(handler).toHaveBeenLastCalledWith(undefined)
  })

  it('unsubscribes from RPC calls', async () => {
    const handler = jest.fn((response) => response)

    const filter1 = {
      args: { args: 'abc' },
      request: { scope: 'test', procedure: 'procedure1', version: '1' },
    }
    const filter2 = {
      request: { scope: 'test', procedure: 'procedure1', version: '2' },
    }

    rpc.subscribe(filter1, handler)
    rpc.subscribe(filter2, handler)

    await rpc.call(filter1.request, { args: 'abc' })
    await rpc.call(filter2.request)

    rpc.unsubscribe(filter1, handler)
    rpc.unsubscribe(filter2, handler)

    await rpc.call(filter1.request, { args: 'abc' })
    await rpc.call(filter2.request)

    expect(handler).toHaveBeenCalled()
    expect(handler).toHaveBeenCalledTimes(2)
  })
})
