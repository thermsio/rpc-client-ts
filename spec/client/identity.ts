import { RPCClient } from '../../src/RPCClient'
import { MockHTTPTransport } from './test-utils/MockHTTPTransport'

describe('rpc client identity', () => {
  it('can set identity and it is sent with each request', async () => {
    const rpc = new RPCClient({
      transports: {
        http: new MockHTTPTransport(),
      },
    })

    const identity = {
      authorization: 'auth123',
    }

    rpc.setIdentity(identity)

    expect(rpc.getIdentity()).toEqual(identity)

    const res = await rpc.call({
      args: { returnIdentity: true },
      procedure: 'test',
    })

    expect(res.data).toEqual(identity)
  })

  it('can set identity.metadata', async () => {
    const rpc = new RPCClient({
      transports: {
        http: new MockHTTPTransport(),
      },
    })

    const metadata = {
      someDetails: ['1', 'a', 'b']
    }

    rpc.setIdentityMetadata(metadata)

    expect(rpc.getIdentity()?.metadata).toEqual(metadata)

    const res = await rpc.call({
      args: { returnIdentity: true },
      procedure: 'test',
    })

    expect(res.data.metadata).toEqual(metadata)
  })

  it('can override identity on a single request', async () => {
    const rpc = new RPCClient({
      transports: {
        http: new MockHTTPTransport(),
      },
    })

    const identity = {
      authorization: 'the_set_identity',
    }

    rpc.setIdentity(identity)

    expect(rpc.getIdentity()).toEqual(identity)

    const resWithTempIdentity = await rpc.call({
      args: { returnIdentity: true },
      identity: {
        authorization: 'the_temp_identity',
      },
      procedure: 'test',
    })

    expect(resWithTempIdentity.data).toEqual({
      authorization: 'the_temp_identity',
    })

    const resWithOriginalIdentity = await rpc.call({
      args: { returnIdentity: true },
      procedure: 'test',
    })

    expect(resWithOriginalIdentity.data).toEqual(identity)
  })
})
