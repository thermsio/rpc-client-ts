import stringify from 'fast-json-stable-stringify';
import { cloneDeep, merge } from 'lodash-es';
import { LRUCache } from 'lru-cache';

/******************************************************************************
Copyright (c) Microsoft Corporation.

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.
***************************************************************************** */
/* global Reflect, Promise, SuppressedError, Symbol, Iterator */


function __awaiter(thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
}

function __values(o) {
    var s = typeof Symbol === "function" && Symbol.iterator, m = s && o[s], i = 0;
    if (m) return m.call(o);
    if (o && typeof o.length === "number") return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
    throw new TypeError(s ? "Object is not iterable." : "Symbol.iterator is not defined.");
}

function __asyncValues(o) {
    if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
    var m = o[Symbol.asyncIterator], i;
    return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
    function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
    function settle(resolve, reject, d, v) { Promise.resolve(v).then(function(v) { resolve({ value: v, done: d }); }, reject); }
}

typeof SuppressedError === "function" ? SuppressedError : function (error, suppressed, message) {
    var e = new Error(message);
    return e.name = "SuppressedError", e.error = error, e.suppressed = suppressed, e;
};

const DEFAULT_REQUEST_SCOPE = 'global';
const DEFAULT_REQUEST_VERSION = '1';
class CallRequestDTO {
    constructor(call) {
        const { args, correlationId, identity, procedure, scope, version } = call;
        this.args = args;
        if (correlationId && typeof correlationId !== 'string')
            throw new Error('correlationId must be a string');
        this.correlationId = correlationId || `${Date.now()}-${Math.random().toString()}`;
        if (identity) {
            if (typeof identity !== 'object')
                throw new Error('identity must be an object');
            if (identity.authorization && typeof identity.authorization !== 'string')
                throw new Error('identity.authorization must be a string');
            if (identity.deviceName && typeof identity.deviceName !== 'string')
                throw new Error('identity.deviceName must be a string');
            if (identity.metadata && typeof identity.metadata !== 'object')
                throw new Error('identity.metadata must be a object');
            this.identity = identity;
        }
        if (procedure && typeof procedure !== 'string')
            throw new Error('procedure must be string');
        this.procedure = procedure;
        if (scope && typeof scope !== 'string')
            throw new Error('scope must be string');
        this.scope = scope;
        if (version && typeof version !== 'string')
            throw new Error('version must be string');
        this.version = version;
    }
}

function isPlainObject(value) {
    if (Object.prototype.toString.call(value) !== '[object Object]') {
        return false;
    }
    const prototype = Object.getPrototypeOf(value);
    return prototype === null || prototype === Object.prototype;
}
function sortKeys(object, options = {}) {
    if (!isPlainObject(object) && !Array.isArray(object)) {
        throw new TypeError('Expected a plain object or array');
    }
    const { deep, compare } = options;
    const seenInput = [];
    const seenOutput = [];
    const deepSortArray = (array) => {
        const seenIndex = seenInput.indexOf(array);
        if (seenIndex !== -1) {
            return seenOutput[seenIndex];
        }
        const result = [];
        seenInput.push(array);
        seenOutput.push(result);
        result.push(...array.map((item) => {
            if (Array.isArray(item)) {
                return deepSortArray(item);
            }
            if (isPlainObject(item)) {
                return _sortKeys(item);
            }
            return item;
        }));
        return result;
    };
    const _sortKeys = (object) => {
        const seenIndex = seenInput.indexOf(object);
        if (seenIndex !== -1) {
            return seenOutput[seenIndex];
        }
        const result = {};
        const keys = Object.keys(object).sort(compare);
        seenInput.push(object);
        seenOutput.push(result);
        for (const key of keys) {
            const value = object[key];
            let newValue;
            if (deep && Array.isArray(value)) {
                newValue = deepSortArray(value);
            }
            else {
                newValue = deep && isPlainObject(value) ? _sortKeys(value) : value;
            }
            Object.defineProperty(result, key, Object.assign(Object.assign({}, Object.getOwnPropertyDescriptor(object, key)), { value: newValue }));
        }
        return result;
    };
    if (Array.isArray(object)) {
        return deep ? deepSortArray(object) : object.slice();
    }
    return _sortKeys(object);
}

const makeCallRequestKey = (request) => {
    if (!request.args)
        return `${request.scope}${request.procedure}${request.version}`;
    return `${request.scope}${request.procedure}${request.version}${stringify(sortKeys(request.args || {}, { deep: true }))}`;
};

class CallResponseDTO {
    constructor(response) {
        const { code, correlationId, data, message, success } = response;
        if (typeof code !== 'number')
            throw new Error('code must be a number');
        this.code = code;
        if (correlationId && typeof correlationId !== 'string')
            throw new Error('correlationId must be a string');
        this.correlationId = correlationId;
        this.data = data;
        if (message && typeof message !== 'string')
            throw new Error('message must be a string');
        this.message = message;
        if (typeof success !== 'boolean')
            throw new Error('success must be a boolean');
        this.success = success;
    }
}
class CallResponseError extends Error {
    constructor(response, request, callOptions) {
        super(response.message);
        this.response = {
            code: 0,
            success: false,
        };
        this.name = 'CallResponseError';
        const { code, correlationId, message } = response;
        this.response.code = code;
        this.response.correlationId = correlationId;
        this.response.data = response.data;
        this.response.message =
            `${makeCallRequestKey(request)} response error: ` +
                (message || 'RPC call failed with no message');
        this.request = request;
        this.callOptions = callOptions;
    }
}

class CallRequestError extends Error {
}

let lsDebug = null;
try {
    lsDebug = localStorage.getItem('debug') || '';
}
catch (e) {
    if (typeof window !== 'undefined' && typeof localStorage !== 'undefined') {
        console.warn('Error checking window.debug');
    }
}
const GetDebugLogger = (prefix) => {
    if (lsDebug) {
        if (new RegExp(lsDebug).test(prefix)) {
            return (...args) => console.log(prefix, ...args);
        }
    }
    return (() => undefined);
};

class CallRequestTransportTimeoutError extends Error {
}
class CallRequestTransportError extends Error {
}

const debug$4 = GetDebugLogger('rpc:ClientManager');
class ClientManager {
    constructor(options) {
        this.options = options;
        this.inflightCallsByKey = {};
        this.interceptors = {
            request: [],
            response: [],
        };
        this.addResponseInterceptor = (interceptor) => {
            if (typeof interceptor !== 'function')
                throw new Error('cannot add interceptor that is not a function');
            this.interceptors.response.push(interceptor);
            return () => {
                this.interceptors.response = this.interceptors.response.filter((cb) => cb !== interceptor);
            };
        };
        this.addRequestInterceptor = (interceptor) => {
            if (typeof interceptor !== 'function')
                throw new Error('cannot add interceptor that is not a function');
            this.interceptors.request.push(interceptor);
            return () => {
                this.interceptors.request = this.interceptors.request.filter((cb) => cb !== interceptor);
            };
        };
        this.clearCache = (request) => {
            debug$4('clearCache');
            if (!this.cache)
                return;
            this.cache.clearCache(request);
        };
        this.getCachedResponse = (request) => {
            var _a;
            debug$4('getCachedResponse', request);
            return (_a = this === null || this === void 0 ? void 0 : this.cache) === null || _a === void 0 ? void 0 : _a.getCachedResponse(request);
        };
        this.getInFlightCallCount = () => {
            debug$4('getInFlightCallCount');
            return Object.keys(this.inflightCallsByKey).length;
        };
        this.manageClientRequest = (originalRequest, opts) => __awaiter(this, void 0, void 0, function* () {
            debug$4('manageClientRequest', originalRequest);
            const request = yield this.interceptRequestMutator(originalRequest);
            const key = makeCallRequestKey(request);
            if (this.inflightCallsByKey.hasOwnProperty(key)) {
                debug$4('manageClientRequest using an existing in-flight call', key);
                return this.inflightCallsByKey[key].then((res) => {
                    const response = cloneDeep(res);
                    if (originalRequest.correlationId) {
                        response.correlationId = originalRequest.correlationId;
                    }
                    return response;
                });
            }
            const requestPromiseWrapper = () => __awaiter(this, void 0, void 0, function* () {
                var _a, _b;
                const preferredTransport = (_a = this.options.transportOptions) === null || _a === void 0 ? void 0 : _a.preferredTransport;
                const useTransport = (_b = opts === null || opts === void 0 ? void 0 : opts.transport) !== null && _b !== void 0 ? _b : preferredTransport;
                const transportResponse = yield this.sendRequestWithTransport(request, Object.assign(Object.assign({}, opts), { transport: useTransport }));
                const response = yield this.interceptResponseMutator(transportResponse, request);
                if (this.cache && response) {
                    this.cache.setCachedResponse(request, response);
                }
                return response;
            });
            const requestPromise = requestPromiseWrapper().finally(() => {
                delete this.inflightCallsByKey[key];
            });
            this.inflightCallsByKey[key] = requestPromise;
            return yield requestPromise;
        });
        this.setIdentity = (identity) => {
            debug$4('setIdentity', identity);
            this.transports.forEach((transport) => transport.setIdentity(identity));
        };
        this.interceptRequestMutator = (originalRequest) => __awaiter(this, void 0, void 0, function* () {
            var _a, e_1, _b, _c;
            let request = originalRequest;
            if (this.interceptors.request.length) {
                debug$4('interceptRequestMutator(), original request:', originalRequest);
                try {
                    for (var _d = true, _e = __asyncValues(this.interceptors.request), _f; _f = yield _e.next(), _a = _f.done, !_a; _d = true) {
                        _c = _f.value;
                        _d = false;
                        const interceptor = _c;
                        request = yield interceptor(request);
                    }
                }
                catch (e_1_1) { e_1 = { error: e_1_1 }; }
                finally {
                    try {
                        if (!_d && !_a && (_b = _e.return)) yield _b.call(_e);
                    }
                    finally { if (e_1) throw e_1.error; }
                }
            }
            return request;
        });
        this.interceptResponseMutator = (originalResponse, request) => __awaiter(this, void 0, void 0, function* () {
            let response = originalResponse;
            if (this.interceptors.response.length) {
                debug$4('interceptResponseMutator', request, originalResponse);
                for (const interceptor of this.interceptors.response) {
                    try {
                        response = yield interceptor(response, request);
                    }
                    catch (e) {
                        debug$4('caught response interceptor, request:', request, 'original response:', originalResponse, 'mutated response:', response);
                        throw e;
                    }
                }
            }
            return response;
        });
        this.sendRequestWithTransport = (request, opts) => __awaiter(this, void 0, void 0, function* () {
            var _a, _b, _c, _d;
            debug$4('sendRequestWithTransport', request);
            let response;
            let transportsIndex = 0;
            let transports = this.transports;
            if (opts === null || opts === void 0 ? void 0 : opts.transport) {
                const transportToUse = this.transports.find((transport) => transport.type === opts.transport);
                if (!transportToUse) {
                    throw new CallRequestError(`Specified Transport "${opts.transport}" not available`);
                }
                transports = [transportToUse];
            }
            while (!response && transports[transportsIndex]) {
                const transport = transports[transportsIndex];
                if (!transport.isConnected()) {
                    transportsIndex++;
                    debug$4(`sendRequestWithTransport transport ${transport.name} not connected, trying next transport "${(_b = (_a = transports[transportsIndex]) === null || _a === void 0 ? void 0 : _a.name) !== null && _b !== void 0 ? _b : 'no next transport'}"`);
                    continue;
                }
                debug$4(`sendRequestWithTransport trying ${transport.name}`, request);
                try {
                    let timeout;
                    const transportPromise = transport.sendRequest(request, opts || {});
                    const deadlinePromise = new Promise((_, reject) => {
                        timeout = setTimeout(() => {
                            reject(new CallRequestTransportTimeoutError(`Procedure ${request.procedure}`));
                        }, (opts === null || opts === void 0 ? void 0 : opts.timeout) || this.options.deadlineMs);
                    });
                    response = (yield Promise.race([
                        deadlinePromise,
                        transportPromise,
                    ]).finally(() => {
                        clearTimeout(timeout);
                    }));
                }
                catch (e) {
                    (_d = (_c = this.options).onTransportRequestError) === null || _d === void 0 ? void 0 : _d.call(_c, e, transports[transportsIndex].type, request);
                    if (e instanceof CallRequestTransportTimeoutError ||
                        e instanceof CallRequestTransportError) {
                        console.warn(`RPCClient ClientManager#sendRequestWithTransport() sending request with ${transports[transportsIndex].name} failed ${transports[transportsIndex + 1]
                            ? `trying next transport "${transports[transportsIndex + 1].name}"`
                            : 'no more transports to try'}`);
                        transportsIndex++;
                    }
                    else {
                        throw e;
                    }
                }
            }
            if (!response && !this.transports[transportsIndex]) {
                console.error(`RPCClient ClientManager#sendRequestWithTransport() ${request.scope}::${request.procedure}::${request.version} did not get a response from any transports`);
                throw new CallRequestError(`Procedure ${request.procedure} did not get a response from any transports`);
            }
            else if (!response) {
                throw new CallRequestError(`Procedure ${request.procedure} did not get a response from the remote`);
            }
            return response;
        });
        this.cache = options.cache;
        if (options.requestInterceptor) {
            this.interceptors.request.push(options.requestInterceptor);
        }
        if (options.responseInterceptor) {
            this.interceptors.response.push(options.responseInterceptor);
        }
        this.transports = options.transports;
    }
}

class BrowserCache {
    constructor(cacheOptions) {
        this.cacheOptions = cacheOptions;
    }
    clearCache() {
    }
    getCachedResponse(request) {
        return undefined;
    }
    setCachedResponse(request, response) {
    }
}
BrowserCache.DEFAULT_CACHE_MAX_AGE_MS = 1000 * 60 * 5;
BrowserCache.DEFAULT_CACHE_MAX_SIZE = 100;

const debug$3 = GetDebugLogger('rpc:InMemoryCache');
class InMemoryCache {
    constructor(cacheOptions) {
        this.clearCache = (request) => {
            var _a, _b;
            debug$3('clearCache');
            if (request) {
                (_a = this.cachedResponseByParams) === null || _a === void 0 ? void 0 : _a.delete(makeCallRequestKey(request));
                return;
            }
            (_b = this.cachedResponseByParams) === null || _b === void 0 ? void 0 : _b.clear();
        };
        this.getCachedResponse = (request) => {
            var _a;
            debug$3('getCachedResponse, key: ', request);
            let cachedResponse = (_a = this.cachedResponseByParams) === null || _a === void 0 ? void 0 : _a.get(makeCallRequestKey(request));
            if (typeof cachedResponse === 'string') {
                cachedResponse = JSON.parse(cachedResponse);
            }
            return cachedResponse;
        };
        this.setCachedResponse = (request, response) => {
            var _a, _b;
            debug$3('setCachedResponse', request, response);
            const requestKey = makeCallRequestKey(request);
            if (!response) {
                return (_a = this.cachedResponseByParams) === null || _a === void 0 ? void 0 : _a.delete(requestKey);
            }
            const cachedResponse = Object.assign({}, response);
            delete cachedResponse.correlationId;
            (_b = this.cachedResponseByParams) === null || _b === void 0 ? void 0 : _b.set(requestKey, cachedResponse ? JSON.stringify(cachedResponse) : undefined);
        };
        this.cachedResponseByParams = new LRUCache({
            max: (cacheOptions === null || cacheOptions === void 0 ? void 0 : cacheOptions.cacheMaxSize) || InMemoryCache.DEFAULT_CACHE_MAX_SIZE,
            ttl: (cacheOptions === null || cacheOptions === void 0 ? void 0 : cacheOptions.cacheMaxAgeMs) || InMemoryCache.DEFAULT_CACHE_MAX_AGE_MS,
        });
    }
}
InMemoryCache.DEFAULT_CACHE_MAX_AGE_MS = 1000 * 60 * 15;
InMemoryCache.DEFAULT_CACHE_MAX_SIZE = 200;

var TransportType;
(function (TransportType) {
    TransportType["http"] = "http";
    TransportType["websocket"] = "websocket";
})(TransportType || (TransportType = {}));

const getRequestShorthand = (request) => {
    return `${request.scope}::${request.procedure}::${request.version}`;
};
const parseRequestShorthand = (requestString) => {
    const parsed = requestString.split('::');
    return {
        procedure: parsed[1],
        scope: parsed[0] || DEFAULT_REQUEST_SCOPE,
        version: parsed[2] || DEFAULT_REQUEST_VERSION,
    };
};

const debug$2 = GetDebugLogger('rpc:HTTPTransport');
class HTTPTransport {
    constructor(options) {
        this.options = options;
        this.name = 'HttpTransport';
        this.type = TransportType.http;
        this.isConnected = () => {
            var _a;
            if (typeof window !== 'undefined') {
                return !!((_a = window === null || window === void 0 ? void 0 : window.navigator) === null || _a === void 0 ? void 0 : _a.onLine);
            }
            return typeof fetch === 'function';
        };
        this.sendRequest = (call, opts) => __awaiter(this, void 0, void 0, function* () {
            debug$2('sendRequest', call);
            try {
                const body = JSON.stringify(Object.assign({ identity: this.identity }, call));
                const res = yield fetch(this.host, {
                    body,
                    cache: 'default',
                    credentials: 'omit',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    method: 'POST',
                    mode: 'cors',
                    redirect: 'follow',
                    referrerPolicy: 'origin',
                });
                const string = yield res.text();
                if (!string)
                    throw new Error('No response received from remote');
                const response = JSON.parse(string);
                return new CallResponseDTO(response);
            }
            catch (err) {
                debug$2('sendRequest() error', err);
                throw new CallRequestTransportError(`HTTPTransport request error ${getRequestShorthand(call)} (${err === null || err === void 0 ? void 0 : err.message})`);
            }
        });
        this.setIdentity = (identity) => {
            debug$2('setIdentity', identity);
            this.identity = identity;
        };
        this.host = options.host;
    }
}

const DEFAULT_TIMEOUT = 30000;
class PromiseWrapper {
    constructor(description, timeout) {
        this.reject = (rejectReturnValue) => {
            if (this.rejectPromise) {
                this.rejectPromise(rejectReturnValue);
            }
        };
        this.resolve = (resolveReturnValue) => {
            if (this.resolvePromise) {
                this.resolvePromise(resolveReturnValue);
            }
        };
        this.promise = new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
            const timer = setTimeout(() => {
                reject(new Error(`PromiseWraper timeout: ${description}`));
            }, timeout || DEFAULT_TIMEOUT);
            this.rejectPromise = (arg) => {
                clearTimeout(timer);
                reject(arg);
            };
            this.resolvePromise = (arg) => {
                clearTimeout(timer);
                resolve(arg);
            };
        }));
    }
}

const sleep = (ms) => new Promise((r) => setTimeout(r, ms));

const debug$1 = GetDebugLogger('rpc:WebSocketTransport');
class WebSocketTransport {
    constructor(options) {
        var _a, _b;
        this.options = options;
        this.connectedToRemote = false;
        this.isWaitingForIdentityConfirmation = false;
        this.pendingPromisesForResponse = {};
        this.serverMessageHandlers = [];
        this.websocketId = undefined;
        this.name = 'WebSocketTransport';
        this.type = TransportType.websocket;
        this.isConnected = () => {
            var _a;
            return ((_a = this.websocket) === null || _a === void 0 ? void 0 : _a.readyState) === WebSocket.OPEN;
        };
        this.connect = () => {
            this._connect();
        };
        this.disconnect = () => {
            this._disconnect('public disconnect() called');
        };
        this.sendClientMessageToServer = (msg) => {
            var _a, _b;
            let strMsg = msg;
            try {
                strMsg = JSON.stringify({ clientMessage: msg });
            }
            catch (err) {
                console.error('WebSocketTransport.sendClientMessage() unable to stringify "msg"', msg);
                return;
            }
            debug$1('sendClientMessageToServer', strMsg);
            if (((_a = this.websocket) === null || _a === void 0 ? void 0 : _a.readyState) === WebSocket.OPEN) {
                (_b = this.websocket) === null || _b === void 0 ? void 0 : _b.send(strMsg);
            }
            else {
                debug$1('WebSocket is not connected. Message not sent:', strMsg);
            }
        };
        this.sendRequest = (call, opts) => __awaiter(this, void 0, void 0, function* () {
            var _a;
            debug$1('sendRequest', call);
            if (((_a = this.websocket) === null || _a === void 0 ? void 0 : _a.readyState) !== WebSocket.OPEN) {
                debug$1('sendRequest WebSocket is not connected');
                throw new CallRequestTransportError('WebSocket is not connected');
            }
            let waitCount = 0;
            const maxWaitAttempts = 20;
            while (this.isWaitingForIdentityConfirmation) {
                debug$1('waiting for identity confirmation');
                waitCount++;
                if (waitCount >= maxWaitAttempts) {
                    throw new CallRequestTransportError('Timed out waiting for identity confirmation');
                }
                yield sleep(100);
            }
            return this.sendCall(call, opts);
        });
        this.setIdentity = (identity) => __awaiter(this, void 0, void 0, function* () {
            debug$1('setIdentity', identity);
            if (!identity) {
                this.identity = undefined;
                yield this.resetConnection();
                return;
            }
            else {
                this.identity = identity;
            }
            if (!this.connectedToRemote) {
                debug$1('setIdentity is not connected to remote');
                return;
            }
            else if (this.isWaitingForIdentityConfirmation) {
                debug$1('setIdentity returning early because "this.isWaitingForIdentityConfirmation=true"');
                return;
            }
            this.isWaitingForIdentityConfirmation = true;
            this.sendCall({ identity }, {})
                .then(() => {
                debug$1('setIdentity with remote complete');
            })
                .catch((e) => {
                debug$1('setIdentity with remote error', e);
            })
                .finally(() => {
                this.isWaitingForIdentityConfirmation = false;
            });
        });
        this.subscribeToServerMessages = (handler) => {
            this.serverMessageHandlers.push(handler);
            return () => {
                this.serverMessageHandlers = this.serverMessageHandlers.filter((cb) => cb !== handler);
            };
        };
        this.unsubscribeFromServerMessages = (handler) => {
            this.serverMessageHandlers = this.serverMessageHandlers.filter((_handler) => _handler !== handler);
        };
        this._connect = () => {
            debug$1('connect', this.host);
            if (this.websocket) {
                debug$1('connect() returning early, websocket already exists');
                return;
            }
            this.websocketId = Math.random();
            this.websocket = new WebSocket(this.host);
            const ws = this.websocket;
            ws.onopen = () => {
                console.info(`[${new Date().toLocaleTimeString()}] WebSocket connected`);
                this.setConnectedToRemote(true);
            };
            ws.onmessage = (msg) => {
                this.handleWebSocketMsg(msg);
            };
            ws.onclose = (e) => {
                if (this.connectedToRemote) {
                    console.info(`[${new Date().toLocaleTimeString()}] WebSocket closed`, e.reason || e.code);
                }
                else {
                    debug$1('WebSocket closed, it was not connected to the remote');
                }
                this.setConnectedToRemote(false);
                this.isWaitingForIdentityConfirmation = false;
                this.websocket = undefined;
                this.websocketId = undefined;
                Object.entries(this.pendingPromisesForResponse).forEach(([correlationId, promiseWrapper]) => {
                    promiseWrapper.reject(new CallRequestTransportError('Websocket closed'));
                });
                if (!e.wasClean) {
                    setTimeout(() => {
                        this.connect();
                    }, 1000);
                }
            };
            ws.onerror = (err) => {
                if (this.connectedToRemote) {
                    console.error('WebSocket encountered error: ', err);
                }
                else {
                    debug$1('WebSocket errored, it was not connected to the remote');
                }
                ws.close();
            };
        };
        this._disconnect = (reason) => __awaiter(this, void 0, void 0, function* () {
            var _a;
            debug$1('_disconnect');
            this.setConnectedToRemote(false);
            this.isWaitingForIdentityConfirmation = false;
            (_a = this.websocket) === null || _a === void 0 ? void 0 : _a.close(1000, reason);
            this.websocket = undefined;
            this.websocketId = undefined;
        });
        this.handleServerMessage = (msg) => {
            this.serverMessageHandlers.forEach((handler) => {
                try {
                    handler(msg.serverMessage);
                }
                catch (err) {
                    console.warn(`WebSocketTransport.handleServerMessage() a serverMessageHandler errored when calling`, msg, 'handler func:', handler);
                    console.error(err);
                }
            });
        };
        this.handleWebSocketMsg = (msg) => {
            debug$1('handleWebSocketMsg', msg);
            let json;
            try {
                json = JSON.parse(msg.data);
            }
            catch (e) {
                debug$1('error parsing WS msg', e);
                return;
            }
            if (json && 'serverMessage' in json) {
                this.handleServerMessage(json);
                return;
            }
            const callResponseDTO = new CallResponseDTO(json);
            if (!callResponseDTO.correlationId) {
                console.error('RPCClient WebSocketTransport received unexpected msg from the server, not correlationId found in response.', json);
                return;
            }
            const pendingRequestPromise = this.pendingPromisesForResponse[callResponseDTO.correlationId];
            if (pendingRequestPromise) {
                pendingRequestPromise.resolve(callResponseDTO);
            }
            else {
                console.warn("rcvd WS msg/response that doesn't match any pending RPC's", json);
            }
        };
        this.resetConnection = () => __awaiter(this, void 0, void 0, function* () {
            debug$1('resetConnection');
            const wasConnected = this.isConnected();
            yield this._disconnect('WebSocketTransport#resetConnection');
            if (wasConnected) {
                yield sleep(300);
                yield this._connect();
            }
            return this.isConnected();
        });
        this.sendCall = (call, opts) => __awaiter(this, void 0, void 0, function* () {
            var _a;
            debug$1('sendCall', call);
            const correlationId = `${Date.now()}-${Math.random().toString()}`;
            call.correlationId = call.correlationId || correlationId;
            const promiseWrapper = new PromiseWrapper(getRequestShorthand(call), opts.timeout || this.options.rpcOptions.deadlineMs);
            if (!this.isConnected()) {
                const error = new CallRequestTransportError('Cannot send message: WebSocket is not connected');
                debug$1('sendCall failed - websocket not connected', error);
                promiseWrapper.reject(error);
                return promiseWrapper.promise;
            }
            (_a = this.websocket) === null || _a === void 0 ? void 0 : _a.send(JSON.stringify(call));
            this.pendingPromisesForResponse[call.correlationId] = promiseWrapper;
            return yield promiseWrapper.promise.finally(() => {
                delete this.pendingPromisesForResponse[call.correlationId];
            });
        });
        this.setConnectedToRemote = (connected) => {
            debug$1(`setConnectedToRemote: ${connected}`);
            this.connectedToRemote = connected;
            if (this.options.onConnectionStatusChange) {
                this.options.onConnectionStatusChange(connected);
            }
            if (connected && this.identity) {
                this.setIdentity(this.identity);
            }
        };
        debug$1('new WebSocketTransport()');
        this.host = options.host;
        if (((_b = (_a = options.rpcOptions.transportOptions) === null || _a === void 0 ? void 0 : _a.websocket) === null || _b === void 0 ? void 0 : _b.autoConnect) !== false) {
            this.connect();
        }
    }
}

class Vent {
    constructor() {
        this.events = {};
        this.publish = (topic, data) => __awaiter(this, void 0, void 0, function* () {
            const handlers = this.events[topic];
            if (!handlers)
                return;
            handlers.forEach(function (handler) {
                handler.call(handler, data);
            });
        });
        this.subscribe = (topic, handler) => {
            if (!this.events[topic]) {
                this.events[topic] = [];
            }
            this.events[topic].push(handler);
            return () => this.unsubscribe(topic, handler);
        };
        this.unsubscribe = (topic, handler) => {
            if (!this.events[topic])
                return;
            let handlerIdx = this.events[topic].indexOf(handler);
            this.events[topic].splice(handlerIdx);
        };
    }
}

const debug = require('debug')('rpc:RPCClient');
const ERRORS = {
    HTTP_HOST_OR_TRANSPORT_REQUIRED: `http host or tansport is required`,
    INTERCEPTOR_MUSTBE_FUNC: `interceptors must be a function`,
};
class RPCClient {
    constructor(options) {
        var _a, _b, _c, _d, _e, _f;
        this.options = options;
        this.webSocketConnectionChangeListeners = [];
        this.vent = new Vent();
        this.onWebSocketConnectionStatusChange = (connected) => {
            if (this.options.onWebSocketConnectionStatusChange) {
                this.options.onWebSocketConnectionStatusChange(connected);
            }
            this.webSocketConnectionChangeListeners.forEach((cb) => cb(connected));
        };
        this.call = (request, args, opts) => __awaiter(this, void 0, void 0, function* () {
            if (!request)
                throw new Error('RPCClient.call(request) requires a "request" param');
            let req;
            if (typeof request === 'string')
                req = parseRequestShorthand(request);
            else
                req = request;
            if (args) {
                req.args = args;
            }
            const requestDTO = new CallRequestDTO(req);
            if (!requestDTO.procedure && !requestDTO.identity) {
                throw new TypeError('RPCClient#call requires a "identity" or "procedure" prop and received neither');
            }
            if (!requestDTO.identity)
                requestDTO.identity = {};
            requestDTO.identity = merge(Object.assign({}, this.identity), requestDTO.identity);
            const callResponse = yield this.callManager.manageClientRequest(requestDTO, opts);
            if (!callResponse.success) {
                throw new CallResponseError(callResponse, requestDTO, opts);
            }
            const callRequestKey = makeCallRequestKey(req);
            this.vent.publish(callRequestKey, callResponse === null || callResponse === void 0 ? void 0 : callResponse.data);
            return callResponse;
        });
        this.clearCache = (request) => {
            this.callManager.clearCache(request);
        };
        this.getCallCache = (request, args) => {
            let req;
            if (typeof request === 'string')
                req = parseRequestShorthand(request);
            else
                req = request;
            if (args) {
                req.args = args;
            }
            const cachedResponse = this.callManager.getCachedResponse(req);
            if (cachedResponse)
                return cachedResponse;
            return undefined;
        };
        this.getIdentity = () => this.identity ? cloneDeep(this.identity) : this.identity;
        this.getInFlightCallCount = () => {
            return this.callManager.getInFlightCallCount();
        };
        this.getWebSocketConnected = () => {
            var _a;
            return !!((_a = this === null || this === void 0 ? void 0 : this.webSocketTransport) === null || _a === void 0 ? void 0 : _a.isConnected());
        };
        this.makeProcedure = (request) => {
            const self = this;
            let req;
            if (typeof request === 'string')
                req = parseRequestShorthand(request);
            else
                req = request;
            return function curriedProcedure(args) {
                return self.call(Object.assign(Object.assign({}, req), { args }));
            };
        };
        this.registerResponseInterceptor = (responseInterceptor) => {
            return this.callManager.addResponseInterceptor(responseInterceptor);
        };
        this.registerRequestInterceptor = (requestInterceptor) => {
            return this.callManager.addRequestInterceptor(requestInterceptor);
        };
        this.registerWebSocketConnectionStatusChangeListener = (cb) => {
            this.webSocketConnectionChangeListeners.push(cb);
            return () => {
                this.webSocketConnectionChangeListeners =
                    this.webSocketConnectionChangeListeners.filter((_cb) => _cb !== cb);
            };
        };
        this.sendClientMessageToServer = (msg) => {
            if (!this.webSocketTransport) {
                console.warn('RPCClient.sendClientMessageToServer() unable to send because RPCClient has no websocket configuration');
                return;
            }
            if (this.webSocketTransport.isConnected()) {
                this.webSocketTransport.sendClientMessageToServer(msg);
            }
            else {
                debug('RPCClient.sendClientMessageToServer() cannot send because the websocket is not connected');
            }
        };
        this.setIdentity = (identity) => {
            let newIdentity = cloneDeep(identity);
            this.identity = identity;
            this.callManager.setIdentity(newIdentity);
        };
        this.setIdentityMetadata = (metadata) => {
            var _a;
            (_a = this.identity) !== null && _a !== void 0 ? _a : (this.identity = {});
            const newIdentity = cloneDeep(this.identity);
            newIdentity.metadata = metadata;
            this.identity.metadata = metadata;
            this.callManager.setIdentity(newIdentity);
        };
        this.transports = () => {
            return {
                http: this.httpTransport,
                websocket: this.webSocketTransport,
            };
        };
        if (!((_a = options === null || options === void 0 ? void 0 : options.hosts) === null || _a === void 0 ? void 0 : _a.http) && !((_b = options === null || options === void 0 ? void 0 : options.transports) === null || _b === void 0 ? void 0 : _b.http)) {
            throw new Error(ERRORS.HTTP_HOST_OR_TRANSPORT_REQUIRED);
        }
        if (options.requestInterceptor &&
            typeof options.requestInterceptor !== 'function') {
            throw new Error(ERRORS.INTERCEPTOR_MUSTBE_FUNC);
        }
        if (options.responseInterceptor &&
            typeof options.responseInterceptor !== 'function') {
            throw new Error(ERRORS.INTERCEPTOR_MUSTBE_FUNC);
        }
        let cache;
        const cacheOptions = { cacheMaxAgeMs: options === null || options === void 0 ? void 0 : options.cacheMaxAgeMs };
        if (options.cacheType == 'browser') {
            cache = new BrowserCache(cacheOptions);
        }
        else {
            cache = new InMemoryCache(cacheOptions);
        }
        const transports = [];
        if ((_c = options.transports) === null || _c === void 0 ? void 0 : _c.websocket) {
            transports.push(options.transports.websocket);
        }
        else if ((_d = options === null || options === void 0 ? void 0 : options.hosts) === null || _d === void 0 ? void 0 : _d.websocket) {
            this.webSocketTransport = new WebSocketTransport({
                host: options.hosts.websocket,
                onConnectionStatusChange: this.onWebSocketConnectionStatusChange,
                rpcOptions: options,
            });
            transports.push(this.webSocketTransport);
        }
        if ((_e = options.transports) === null || _e === void 0 ? void 0 : _e.http) {
            transports.push(options.transports.http);
        }
        else if ((_f = options === null || options === void 0 ? void 0 : options.hosts) === null || _f === void 0 ? void 0 : _f.http) {
            this.httpTransport = new HTTPTransport({
                host: options.hosts.http,
                rpcOptions: options,
            });
            transports.push(this.httpTransport);
        }
        this.callManager = new ClientManager({
            cache,
            deadlineMs: options.deadlineMs || RPCClient.DEFAULT_DEADLINE_MS,
            onTransportRequestError: options.onTransportRequestError,
            requestInterceptor: options.requestInterceptor,
            responseInterceptor: options.responseInterceptor,
            transports,
            transportOptions: options.transportOptions,
        });
    }
    subscribe(filter, handler) {
        const request = typeof filter.request === 'string'
            ? parseRequestShorthand(filter.request)
            : filter.request;
        const callRequestKey = makeCallRequestKey(Object.assign(Object.assign({}, request), { args: filter.args }));
        return this.vent.subscribe(callRequestKey, handler);
    }
    unsubscribe(filter, handler) {
        const request = typeof filter.request === 'string'
            ? parseRequestShorthand(filter.request)
            : filter.request;
        const callRequestKey = makeCallRequestKey(Object.assign(Object.assign({}, request), { args: filter.args }));
        this.vent.unsubscribe(callRequestKey, handler);
    }
    subscribeToServerMessages(handler) {
        if (!this.webSocketTransport) {
            console.warn('RPCClient.subscribeToServerMessages() cannot subscribe because RPCClient has no websocket configuration');
            return () => { };
        }
        return this.webSocketTransport.subscribeToServerMessages(handler);
    }
    unsubscribeFromServerMessages(handler) {
        var _a;
        (_a = this.webSocketTransport) === null || _a === void 0 ? void 0 : _a.unsubscribeFromServerMessages(handler);
    }
}
RPCClient.DEFAULT_DEADLINE_MS = 10000;

export { CallRequestTransportError, CallRequestTransportTimeoutError, CallResponseError, RPCClient, CallResponseDTO as RPCResponse, TransportType };
//# sourceMappingURL=index.js.map
