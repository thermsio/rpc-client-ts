export { CallOptions, RPCClient, RPCClientOptions } from './RPCClient'

import { CallRequestDTO } from './CallRequestDTO'
export type RPCRequest = CallRequestDTO | string

export { CallResponseDTO as RPCResponse, CallResponseError } from './CallResponseDTO'
export { RPCClientIdentity } from './RPCClientIdentity'

// @deprecated - remove this export in next major release
export { RPCClientIdentity as RPCIdentity } from './RPCClientIdentity'

export * from './transports/Transport'

export { CallRequestTransportError, CallRequestTransportTimeoutError } from "./transports/errors";
