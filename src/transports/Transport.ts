import { CallRequestDTO } from '../CallRequestDTO'
import { CallResponseDTO } from '../CallResponseDTO'
import { RPCClientIdentity } from '../RPCClientIdentity'
import { CallOptions } from '../RPCClient'

export enum TransportType {
  'http' = 'http',
  'websocket' = 'websocket',
}

export interface Transport {
  isConnected(): boolean

  name: string

  type: TransportType

  sendRequest(
    call: CallRequestDTO,
    opts: CallOptions,
  ): Promise<CallResponseDTO>

  setIdentity(identity?: RPCClientIdentity): void
}
