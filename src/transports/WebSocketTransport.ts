import { Transport, TransportType } from './Transport'
import { CallRequestDTO } from '../CallRequestDTO'
import { CallResponseDTO } from '../CallResponseDTO'
import { RPCClientIdentity } from '../RPCClientIdentity'
import { PromiseWrapper } from '../utils/PromiseWrapper'
import { sleep } from '../utils/sleep'
import { GetDebugLogger } from '../utils/debug-logger'
import { UnsubscribeCallback } from '../types'
import { getRequestShorthand } from '../utils/request'
import { RPCClientOptions, CallOptions } from '../RPCClient'
import { CallRequestTransportError } from './errors'

const debug = GetDebugLogger('rpc:WebSocketTransport')

type ServerMessageHandler = (serverMessage: any) => void

interface WebSocketTransportOptions {
  host: string
  identity?: RPCClientIdentity
  onConnectionStatusChange?: (connected: boolean) => void
  rpcOptions: RPCClientOptions
}

export class WebSocketTransport implements Transport {
  private connectedToRemote: boolean = false
  private readonly host: string
  private identity?: RPCClientIdentity
  private isWaitingForIdentityConfirmation: boolean = false
  private pendingPromisesForResponse: {
    [correlationId: string]: PromiseWrapper<
      CallResponseDTO,
      CallResponseDTO | CallRequestTransportError
    >
  } = {}
  private serverMessageHandlers: ServerMessageHandler[] = []
  private websocket?: WebSocket
  private websocketId?: number = undefined

  readonly name = 'WebSocketTransport'

  readonly type = TransportType.websocket

  constructor(readonly options: WebSocketTransportOptions) {
    debug('new WebSocketTransport()')
    this.host = options.host

    if (options.rpcOptions.transportOptions?.websocket?.autoConnect !== false) {
      this.connect()
    }
  }

  public isConnected = () => {
    return this.websocket?.readyState === WebSocket.OPEN
  }

  public connect = (): void => {
    this._connect()
  }

  public disconnect = (): void => {
    this._disconnect('public disconnect() called')
  }

  public sendClientMessageToServer = (msg: any) => {
    let strMsg = msg

    try {
      strMsg = JSON.stringify({ clientMessage: msg })
    } catch (err) {
      console.error(
        'WebSocketTransport.sendClientMessage() unable to stringify "msg"',
        msg,
      )
      return
    }

    debug('sendClientMessageToServer', strMsg)
    if (this.websocket?.readyState === WebSocket.OPEN) {
      this.websocket?.send(strMsg)
    } else {
      debug('WebSocket is not connected. Message not sent:', strMsg)
    }
  }

  public sendRequest = async (
    call: CallRequestDTO,
    opts: CallOptions,
  ): Promise<CallResponseDTO> => {
    debug('sendRequest', call)

    if (this.websocket?.readyState !== WebSocket.OPEN) {
      debug('sendRequest WebSocket is not connected')
      throw new CallRequestTransportError('WebSocket is not connected')
    }

    let waitCount = 0
    const maxWaitAttempts = 20 // 2 seconds max wait (20 * 100ms)
    while (this.isWaitingForIdentityConfirmation) {
      debug('waiting for identity confirmation')
      waitCount++

      if (waitCount >= maxWaitAttempts) {
        throw new CallRequestTransportError(
          'Timed out waiting for identity confirmation',
        )
      }
      await sleep(100)
    }

    return this.sendCall(call, opts)
  }

  public setIdentity = async (identity?: RPCClientIdentity) => {
    debug('setIdentity', identity)

    if (!identity) {
      this.identity = undefined
      await this.resetConnection()

      return
    } else {
      this.identity = identity
    }

    if (!this.connectedToRemote) {
      debug('setIdentity is not connected to remote')
      return
    } else if (this.isWaitingForIdentityConfirmation) {
      debug(
        'setIdentity returning early because "this.isWaitingForIdentityConfirmation=true"',
      )
      return
    }

    this.isWaitingForIdentityConfirmation = true

    this.sendCall({ identity }, {})
      .then(() => {
        debug('setIdentity with remote complete')
      })
      .catch((e) => {
        debug('setIdentity with remote error', e)
      })
      .finally(() => {
        this.isWaitingForIdentityConfirmation = false
      })
  }

  public subscribeToServerMessages = (
    handler: (msg: any) => void,
  ): UnsubscribeCallback => {
    this.serverMessageHandlers.push(handler)

    return () => {
      this.serverMessageHandlers = this.serverMessageHandlers.filter(
        (cb) => cb !== handler,
      )
    }
  }

  public unsubscribeFromServerMessages = (handler: (msg: any) => void) => {
    this.serverMessageHandlers = this.serverMessageHandlers.filter(
      (_handler) => _handler !== handler,
    )
  }

  private _connect = () => {
    debug('connect', this.host)

    if (this.websocket) {
      debug('connect() returning early, websocket already exists')
      return
    }

    this.websocketId = Math.random()
    this.websocket = new WebSocket(this.host)

    const ws = this.websocket

    ws.onopen = () => {
      console.info(`[${new Date().toLocaleTimeString()}] WebSocket connected`)

      this.setConnectedToRemote(true)
    }

    ws.onmessage = (msg) => {
      this.handleWebSocketMsg(msg)
    }

    ws.onclose = (e) => {
      if (this.connectedToRemote) {
        console.info(
          `[${new Date().toLocaleTimeString()}] WebSocket closed`,
          e.reason || e.code,
        )
      } else {
        debug('WebSocket closed, it was not connected to the remote')
      }

      this.setConnectedToRemote(false)
      this.isWaitingForIdentityConfirmation = false
      this.websocket = undefined
      this.websocketId = undefined

      Object.entries(this.pendingPromisesForResponse).forEach(
        ([correlationId, promiseWrapper]) => {
          promiseWrapper.reject(
            new CallRequestTransportError('Websocket closed'),
          )
        },
      )

      if (!e.wasClean) {
        setTimeout(() => {
          this.connect()
        }, 1000)
      }
    }

    ws.onerror = (err) => {
      if (this.connectedToRemote) {
        console.error('WebSocket encountered error: ', err)
      } else {
        debug('WebSocket errored, it was not connected to the remote')
      }

      ws.close()
    }
  }

  private _disconnect = async (reason?: string) => {
    debug('_disconnect')
    this.setConnectedToRemote(false)
    this.isWaitingForIdentityConfirmation = false
    this.websocket?.close(1000, reason)
    this.websocket = undefined
    this.websocketId = undefined
  }

  private handleServerMessage = (msg: { serverMessage: any }) => {
    this.serverMessageHandlers.forEach((handler) => {
      try {
        handler(msg.serverMessage)
      } catch (err) {
        console.warn(
          `WebSocketTransport.handleServerMessage() a serverMessageHandler errored when calling`,
          msg,
          'handler func:',
          handler,
        )
        console.error(err)
      }
    })
  }

  private handleWebSocketMsg = (msg: MessageEvent) => {
    debug('handleWebSocketMsg', msg)

    let json: any

    try {
      json = JSON.parse(msg.data)
    } catch (e: any) {
      debug('error parsing WS msg', e)
      return // Exit early if JSON parsing fails
    }

    // Check if json is defined and has the serverMessage property
    if (json && 'serverMessage' in json) {
      this.handleServerMessage(json)

      return
    }

    const callResponseDTO = new CallResponseDTO(json)

    if (!callResponseDTO.correlationId) {
      console.error(
        'RPCClient WebSocketTransport received unexpected msg from the server, not correlationId found in response.',
        json,
      )

      return
    }

    const pendingRequestPromise =
      this.pendingPromisesForResponse[callResponseDTO.correlationId]

    if (pendingRequestPromise) {
      pendingRequestPromise.resolve(callResponseDTO)
    } else {
      console.warn(
        "rcvd WS msg/response that doesn't match any pending RPC's",
        json,
      )
    }
  }

  private resetConnection = async () => {
    debug('resetConnection')

    // Store current connection state
    const wasConnected = this.isConnected()

    await this._disconnect('WebSocketTransport#resetConnection')

    if (wasConnected) {
      await sleep(300) // delay to avoid race conditions
      await this._connect()
    }
    return this.isConnected()
  }

  private sendCall = async (
    call: CallRequestDTO,
    opts: CallOptions,
  ): Promise<CallResponseDTO> => {
    debug('sendCall', call)

    // Generate a more robust correlation ID using timestamp + random
    const correlationId = `${Date.now()}-${Math.random().toString()}`

    call.correlationId = call.correlationId || correlationId

    const promiseWrapper = new PromiseWrapper<
      CallResponseDTO,
      CallResponseDTO | CallRequestTransportError
    >(
      getRequestShorthand(call),
      opts.timeout || this.options.rpcOptions.deadlineMs,
    )

    // Check if websocket is connected before sending
    if (!this.isConnected()) {
      const error = new CallRequestTransportError(
        'Cannot send message: WebSocket is not connected',
      )
      debug('sendCall failed - websocket not connected', error)
      promiseWrapper.reject(error)
      return promiseWrapper.promise
    }

    this.websocket?.send(JSON.stringify(call))

    this.pendingPromisesForResponse[call.correlationId] = promiseWrapper

    return await promiseWrapper.promise.finally(() => {
      delete this.pendingPromisesForResponse[call.correlationId!]
    })
  }

  private setConnectedToRemote = (connected: boolean) => {
    debug(`setConnectedToRemote: ${connected}`)
    this.connectedToRemote = connected

    if (this.options.onConnectionStatusChange) {
      this.options.onConnectionStatusChange(connected)
    }

    if (connected && this.identity) {
      this.setIdentity(this.identity)
    }
  }
}
