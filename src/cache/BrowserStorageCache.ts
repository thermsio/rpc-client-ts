import { CallRequestDTO } from '../CallRequestDTO'
import { CallResponseDTO } from '../CallResponseDTO'
import { Cache } from './Cache'

interface BrowserCacheOptions {
  // the max number of responses cached before overwriting
  cacheMaxSize?: number
  // the max time a response is cached for
  cacheMaxAgeMs?: number
  // if false, BrowserCache will use localStorage (if available)
  browserSessionOnly?: boolean
}

export class BrowserCache implements Cache {
  static DEFAULT_CACHE_MAX_AGE_MS = 1000 * 60 * 5
  static DEFAULT_CACHE_MAX_SIZE = 100

  constructor(readonly cacheOptions?: BrowserCacheOptions) {}

  clearCache(): void {
    // todo:
  }

  getCachedResponse(request: CallRequestDTO): CallResponseDTO | undefined {
    // todo:
    return undefined
  }

  setCachedResponse(request: CallRequestDTO, response: CallResponseDTO) {
    // todo:
  }
}
