import { CallResponseDTO } from '../CallResponseDTO'
import { CallRequestDTO } from '../CallRequestDTO'

export interface Cache {
  /** If a request is provided then only that request/response cache is cleared otherwise the entire cache is cleared  */
  clearCache(request?: CallRequestDTO): void
  getCachedResponse(request: CallRequestDTO): CallResponseDTO | undefined
  setCachedResponse(request: CallRequestDTO, response?: CallResponseDTO): void
}
