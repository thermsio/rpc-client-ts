import { CallRequestDTO } from './CallRequestDTO'
import { CallOptions } from './RPCClient'
import { makeCallRequestKey } from './utils/cache-utils'

export interface CallResponseDTO<Data = any> {
  code: number
  correlationId?: string
  data: Data
  message?: string
  success: boolean
}

export class CallResponseDTO<Data = any> {
  code: number
  correlationId?: string
  data: Data
  message?: string
  success: boolean

  constructor(response: CallResponseDTO<Data>) {
    const { code, correlationId, data, message, success } = response

    if (typeof code !== 'number') throw new Error('code must be a number')
    this.code = code

    if (correlationId && typeof correlationId !== 'string')
      throw new Error('correlationId must be a string')
    this.correlationId = correlationId

    this.data = data

    if (message && typeof message !== 'string')
      throw new Error('message must be a string')
    this.message = message

    if (typeof success !== 'boolean')
      throw new Error('success must be a boolean')
    this.success = success
  }
}

export class CallResponseError extends Error {
  readonly response: {
    code: number
    correlationId?: string
    data?: any
    message?: string
    success: false
  } = {
    code: 0,
    success: false,
  }

  readonly request: CallRequestDTO

  readonly callOptions?: CallOptions

  constructor(
    response: CallResponseDTO,
    request: CallRequestDTO,
    callOptions?: CallOptions,
  ) {
    super(response.message)
    this.name = 'CallResponseError'

    const { code, correlationId, message } = response

    this.response.code = code
    this.response.correlationId = correlationId
    this.response.data = response.data
    this.response.message =
      `${makeCallRequestKey(request)} response error: ` +
      (message || 'RPC call failed with no message')

    this.request = request

    this.callOptions = callOptions
  }
}
