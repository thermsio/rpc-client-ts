import { Cache } from '../cache/Cache'
import { Transport, TransportType } from '../transports/Transport'
import { CallRequestDTO } from '../CallRequestDTO'
import { CallResponseDTO } from '../CallResponseDTO'
import { CallRequestError } from '../errors/CallRequestError'
import { CallResponseInterceptor } from '../CallResponseInterceptor'
import { CallRequestInterceptor } from '../CallRequestInterceptor'
import { makeCallRequestKey } from '../utils/cache-utils'
import { RPCClientIdentity } from '../RPCClientIdentity'
import { cloneDeep } from 'lodash-es'
import { GetDebugLogger } from '../utils/debug-logger'
import { UnsubscribeCallback } from '../types'
import { CallOptions, RPCClientOptions } from '../RPCClient'
import {
  CallRequestTransportError,
  CallRequestTransportTimeoutError,
} from '../transports/errors'

const debug = GetDebugLogger('rpc:ClientManager')

export interface ClientManagerOptions {
  cache?: Cache
  deadlineMs: number
  onTransportRequestError?: (
    error: Error | CallRequestTransportError | CallRequestTransportTimeoutError,
    transportType: TransportType,
    request: CallRequestDTO,
  ) => void
  requestInterceptor?: CallRequestInterceptor
  responseInterceptor?: CallResponseInterceptor
  transports: Transport[]
  transportOptions?: RPCClientOptions['transportOptions']
}

export interface ClientManager {
  addResponseInterceptor(
    interceptor: CallResponseInterceptor,
  ): UnsubscribeCallback
  addRequestInterceptor(
    interceptor: CallRequestInterceptor,
  ): UnsubscribeCallback
  clearCache(request?: CallRequestDTO): void
  getCachedResponse(request: CallRequestDTO): CallResponseDTO | undefined
  getInFlightCallCount(): number
  manageClientRequest(
    request: CallRequestDTO,
    opts?: CallOptions,
  ): Promise<CallResponseDTO>
  setIdentity(identity?: RPCClientIdentity): void
}

export class ClientManager implements ClientManager {
  private cache?: Cache
  private inflightCallsByKey: { [key: string]: Promise<CallResponseDTO> } = {}
  private interceptors: {
    request: CallRequestInterceptor[]
    response: CallResponseInterceptor[]
  } = {
    request: [],
    response: [],
  }
  private transports: Transport[]

  constructor(readonly options: ClientManagerOptions) {
    this.cache = options.cache

    if (options.requestInterceptor) {
      this.interceptors.request.push(options.requestInterceptor)
    }

    if (options.responseInterceptor) {
      this.interceptors.response.push(options.responseInterceptor)
    }

    this.transports = options.transports
  }

  public addResponseInterceptor = (interceptor: CallResponseInterceptor) => {
    if (typeof interceptor !== 'function')
      throw new Error('cannot add interceptor that is not a function')
    this.interceptors.response.push(interceptor)

    return () => {
      this.interceptors.response = this.interceptors.response.filter(
        (cb) => cb !== interceptor,
      )
    }
  }

  public addRequestInterceptor = (interceptor: CallRequestInterceptor) => {
    if (typeof interceptor !== 'function')
      throw new Error('cannot add interceptor that is not a function')
    this.interceptors.request.push(interceptor)

    return () => {
      this.interceptors.request = this.interceptors.request.filter(
        (cb) => cb !== interceptor,
      )
    }
  }

  public clearCache = (request?: CallRequestDTO) => {
    debug('clearCache')

    if (!this.cache) return

    this.cache.clearCache(request)
  }

  public getCachedResponse = (request: CallRequestDTO) => {
    debug('getCachedResponse', request)

    return this?.cache?.getCachedResponse(request)
  }

  public getInFlightCallCount = () => {
    debug('getInFlightCallCount')

    return Object.keys(this.inflightCallsByKey).length
  }

  public manageClientRequest = async (
    originalRequest: CallRequestDTO,
    opts?: CallOptions,
  ) => {
    debug('manageClientRequest', originalRequest)

    // todo: wrap w/ try/catch and throw interceptor failed error
    const request = await this.interceptRequestMutator(originalRequest)

    const key = makeCallRequestKey(request)

    if (this.inflightCallsByKey.hasOwnProperty(key)) {
      debug('manageClientRequest using an existing in-flight call', key)

      return this.inflightCallsByKey[key].then((res: CallResponseDTO) => {
        const response = cloneDeep(res)

        if (originalRequest.correlationId) {
          response.correlationId = originalRequest.correlationId
        }

        return response
      })
    }

    const requestPromiseWrapper = async () => {
      const preferredTransport =
        this.options.transportOptions?.preferredTransport

      const useTransport = opts?.transport ?? preferredTransport

      const transportResponse = await this.sendRequestWithTransport(request, {
        ...opts,
        transport: useTransport,
      })

      // todo: wrap w/ try/catch and throw interceptor failed error
      const response = await this.interceptResponseMutator(
        transportResponse,
        request,
      )

      if (this.cache && response) {
        this.cache.setCachedResponse(request, response)
      }

      return response
    }

    const requestPromise = requestPromiseWrapper().finally(() => {
      delete this.inflightCallsByKey[key]
    })

    this.inflightCallsByKey[key] = requestPromise

    return await requestPromise
  }

  public setIdentity = (identity?: RPCClientIdentity) => {
    debug('setIdentity', identity)

    this.transports.forEach((transport) => transport.setIdentity(identity))
  }

  private interceptRequestMutator = async (
    originalRequest: CallRequestDTO,
  ): Promise<CallRequestDTO> => {
    let request = originalRequest

    if (this.interceptors.request.length) {
      debug('interceptRequestMutator(), original request:', originalRequest)

      for await (const interceptor of this.interceptors.request) {
        request = await interceptor(request)
      }
    }

    return request
  }

  private interceptResponseMutator = async (
    originalResponse: CallResponseDTO,
    request: CallRequestDTO,
  ): Promise<CallResponseDTO> => {
    let response = originalResponse

    if (this.interceptors.response.length) {
      debug('interceptResponseMutator', request, originalResponse)

      for (const interceptor of this.interceptors.response) {
        try {
          response = await interceptor(response, request)
        } catch (e) {
          debug(
            'caught response interceptor, request:',
            request,
            'original response:',
            originalResponse,
            'mutated response:',
            response,
          )

          throw e
        }
      }
    }

    return response
  }

  private sendRequestWithTransport = async (
    request: CallRequestDTO,
    opts?: CallOptions,
  ) => {
    debug('sendRequestWithTransport', request)

    let response
    let transportsIndex = 0
    let transports = this.transports

    if (opts?.transport) {
      const transportToUse = this.transports.find(
        (transport) => transport.type === opts.transport,
      )

      if (!transportToUse) {
        throw new CallRequestError(
          `Specified Transport "${opts.transport}" not available`,
        )
      }

      transports = [transportToUse]
    }

    while (!response && transports[transportsIndex]) {
      const transport = transports[transportsIndex]

      if (!transport.isConnected()) {
        transportsIndex++
        debug(
          `sendRequestWithTransport transport ${transport.name} not connected, trying next transport "${transports[transportsIndex]?.name ?? 'no next transport'}"`,
        )
        continue
      }

      debug(`sendRequestWithTransport trying ${transport.name}`, request)

      try {
        let timeout: any

        const transportPromise = transport.sendRequest(request, opts || {})

        const deadlinePromise = new Promise((_, reject) => {
          timeout = setTimeout(() => {
            reject(
              new CallRequestTransportTimeoutError(
                `Procedure ${request.procedure}`,
              ),
            )
          }, opts?.timeout || this.options.deadlineMs)
        })

        response = (await Promise.race([
          deadlinePromise,
          transportPromise,
        ]).finally(() => {
          clearTimeout(timeout)
        })) as Promise<CallResponseDTO>
      } catch (e: any) {
        this.options.onTransportRequestError?.(
          e,
          transports[transportsIndex].type,
          request,
        )

        if (
          e instanceof CallRequestTransportTimeoutError ||
          e instanceof CallRequestTransportError
        ) {
          console.warn(
            `RPCClient ClientManager#sendRequestWithTransport() sending request with ${
              transports[transportsIndex].name
            } failed ${
              transports[transportsIndex + 1]
                ? `trying next transport "${
                    transports[transportsIndex + 1].name
                  }"`
                : 'no more transports to try'
            }`,
          )

          transportsIndex++
        } else {
          throw e
        }
      }
    }

    if (!response && !this.transports[transportsIndex]) {
      console.error(
        `RPCClient ClientManager#sendRequestWithTransport() ${request.scope}::${request.procedure}::${request.version} did not get a response from any transports`,
      )

      throw new CallRequestError(
        `Procedure ${request.procedure} did not get a response from any transports`,
      )
    } else if (!response) {
      throw new CallRequestError(
        `Procedure ${request.procedure} did not get a response from the remote`,
      )
    }

    return response
  }
}
