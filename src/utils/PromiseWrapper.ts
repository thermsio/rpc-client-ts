const DEFAULT_TIMEOUT = 30000

export class PromiseWrapper<T, E> {
  private rejectPromise?: (arg?: E) => void
  private resolvePromise?: (arg: T) => void

  readonly promise: Promise<T>

  constructor(description: string, timeout?: number) {
    this.promise = new Promise(async (resolve, reject) => {
      const timer = setTimeout(() => {
        reject(new Error(`PromiseWraper timeout: ${description}`))
      }, timeout || DEFAULT_TIMEOUT)

      this.rejectPromise = (arg?: E) => {
        clearTimeout(timer)

        reject(arg)
      }

      this.resolvePromise = (arg: T) => {
        clearTimeout(timer)

        resolve(arg)
      }
    })
  }

  reject = (rejectReturnValue?: E): any => {
    if (this.rejectPromise) {
      this.rejectPromise(rejectReturnValue)
    }
  }

  resolve = (resolveReturnValue: T): void => {
    if (this.resolvePromise) {
      this.resolvePromise(resolveReturnValue)
    }
  }
}
