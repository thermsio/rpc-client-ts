import { CallRequestDTO } from '../CallRequestDTO'
import stringify from 'fast-json-stable-stringify'
import sortKeys from './sort-keys'

export const makeCallRequestKey = (request: CallRequestDTO): string => {
  if (!request.args)
    return `${request.scope}${request.procedure}${request.version}`

  return `${request.scope}${request.procedure}${request.version}${stringify(
    sortKeys(request.args || {}, { deep: true }),
  )}`
}
