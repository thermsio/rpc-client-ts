let lsDebug: string | null | undefined = null

try {
  lsDebug = localStorage.getItem('debug') || ''
} catch (e: any) {
  if (typeof window !== 'undefined' && typeof localStorage !== 'undefined') {
    console.warn('Error checking window.debug')
  }
}
export const GetDebugLogger = (prefix: string) => {
  if (lsDebug) {
    if (new RegExp(lsDebug).test(prefix)) {
      return (...args: any[]) => console.log(prefix, ...args)
    }
  }

  return (() => undefined) as (...args: any[]) => void
}
