import {UnsubscribeCallback} from "../types";

export class Vent {
  private events: Record<string, any> = {}

  publish = async (topic: string, data: any) => {
    const handlers = this.events[topic]

    if (!handlers) return

    handlers.forEach(function (handler: (data: any) => void) {
      handler.call(handler, data)
    })
  }

  subscribe = (topic: string, handler: (data: any) => void): UnsubscribeCallback => {
    if (!this.events[topic]) {
      this.events[topic] = []
    }


    this.events[topic].push(handler)

    return () => this.unsubscribe(topic, handler)
  }

  unsubscribe = (topic: string, handler: (data: any) => void) => {
    if (!this.events[topic]) return

    let handlerIdx = this.events[topic].indexOf(handler)

    this.events[topic].splice(handlerIdx)
  }
}
