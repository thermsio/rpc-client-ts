import { CallRequestDTO } from './CallRequestDTO'

export type CallRequestInterceptor = (
  request: CallRequestDTO,
) => Promise<CallRequestDTO> | CallRequestDTO
