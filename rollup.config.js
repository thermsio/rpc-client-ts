import autoExternal from 'rollup-plugin-auto-external'
import typescript from '@rollup/plugin-typescript'
import sourcemaps from 'rollup-plugin-sourcemaps'
import commonjs from 'rollup-plugin-commonjs'
import nodeResolve from 'rollup-plugin-node-resolve'
import terser from '@rollup/plugin-terser'

import pkg from './package.json'

export default [
  {
    input: 'src/index.ts',
    output: [
      {
        file: 'dist/cjs.js',
        format: 'cjs',
        sourcemap: true,
      },
      {
        dir: 'dist/',
        format: 'es',
        sourcemap: true,
      },
    ],
    external: [...Object.keys(pkg.peerDependencies || {})],
    plugins: [
      autoExternal(),
      typescript(),
      // resolve sourcemaps to original code
      sourcemaps(),
    ],
  },
  {
    input: 'src/index.ts',
    output: {
      file: 'dist/umd.js',
      format: 'umd',
      name: 'RPCClient',
    },
    plugins: [
      commonjs(),
      nodeResolve(),
      typescript(),
      // resolve sourcemaps to original code
      sourcemaps(),
      terser(),
    ],
  },
]
