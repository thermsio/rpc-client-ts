window.rpcClient = new RPC.RPCClient({
  hosts: {
    http: 'https://rpc.sandbox.therms.io',
    websocket: 'ws://rpc.sandbox.therms.io/websocket'
    // http: 'http://localhost:3000',
    // websocket: 'ws://localhost:3001'
  },
});

window.makeRPC = (request) => {
  if (!request.correlationId) request.correlationId = Math.random().toString()

  window.addRPCTableRow(request)

  return rpcClient.call(request)
    .then(res => {
      window.updateRPCTableRowWithResponse(res)

      return res
    }).catch((e) => {
      console.error('RPC Client catch(e)', e)

      window.updateRPCTableRowWithResponse({ correlationId: request.correlationId, error: e.message, msg: 'RPC Client threw' })
    })
}


const callRowTemplate = _.template(document.querySelector('#call-row-template').innerHTML)

window.addRPCTableRow = (request) => {
  document.querySelector('tbody').insertAdjacentHTML("beforebegin", callRowTemplate(request))

  const $td = document.querySelector(`.request[data-correlationid="${request.correlationId}"]`)

  JsonView.render(JsonView.createTree(request), $td)
}

window.updateRPCTableRowWithResponse = (response) => {
  if (!response.correlationId) return console.warn('received response w/ no correlationId', response)

  const $td = document.querySelector(`.response[data-correlationid="${response.correlationId}"]`)

  if (!$td) return console.warn('received resposne but no table row can be found')

  if (response.success) {
    $td.classList.add('bg-success')
  } else {
    $td.classList.add('bg-danger')
  }

  try {
    JsonView.render(JsonView.createTree(response), $td)
  } catch (e) {
    console.log('e', e)
    $td.innerHTML = typeof response === 'object' ? JSON.stringify(response) : response
  }
}