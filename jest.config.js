module.exports = {
  moduleNameMapper: {
    '^lodash-es$': 'lodash',
    'objectmodel': 'objectmodel/dist/object-model.cjs'
  },
  preset: 'ts-jest',
  // testEnvironment: 'node',
  testPathIgnorePatterns: ['test-utils'],
  testMatch: ['<rootDir>/spec/**/*.ts']
};
